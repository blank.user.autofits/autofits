<a href="https://gitlab.inesctec.pt/pedro.p.costa/autofits">
    <img src="fits_logo.png" alt="Logo" height="80">
</a>

**AutoFITS** (**Auto**matic **F**eature Extraction from **I**rregular **T**ime-**S**eries) is a Python framework for automatic feature engineering for time-series with unevenly spaced observations in time. 

AutoFITS extracts features from an irregular time-series and uses them in a simple forecasting workflow. Its main focus is complementing standard forecasting methods. For a more detailed explanation on how it works, refer to the original dissertation INSERT LINK.

This package includes 4 models:
- **AutoFITS**: Focused on extracting information from a series' irregularity.
- **BaselineFITS**: AutoFITS without feature creation.
- **VEST**: An adaptation of VEST introduced in [Cerqueira et al.](https://arxiv.org/pdf/2010.07137.pdf).
- **AutoFV**: A model that attempts to combine the feature engineering process from AutoFITS and VEST.

# Installation



# Usage

```python
import pandas as pd
from workflows.auto_fits import AutoFITS
   
df = pd.read_csv('vostok.csv')
df['age (yrs bp)'] = df['age (yrs bp)'].apply(lambda d: pd.Timestamp(1950, 1, 1) + pd.DateOffset(hours=d))

target = 'co2'
schema = {'age (yrs bp)': 'timestamp',
          'co2': 'numeric'}
frequency = '3500H'

af = AutoFITS(schema,
              frequency=frequency,
              target=target,
              resampling_strategy='mean',
              imputation_strategy='ffill')

y = df[target]
X = df.drop(target, axis=1)

af.fit(X, y)
af.forecast()

# R2:  0.8076042832543822
# MAE:  5.688022281784522
# Median AE:  5.237672109215993
# Forecast :  [287.4581584]
# Time window:  (Timestamp('1997-05-19 08:00:00'), Timestamp('1997-10-12 04:00:00'))

```



