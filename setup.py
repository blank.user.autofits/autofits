from setuptools import setup

setup(
    name='AutoFITS',
    version='1.0',
    packages=['workflows', 'data_processing', 'utils'],
    url='https://gitlab.com/blank.user.autofits/autofits',
    license='Apache 2.0',
    author='Blank User',
    description='Automated Feature Engineering for Irregular Time-series'
)
