from logging import warning
from os import path

import numpy as np
import pandas as pd
from metalearn import Metafeatures
from sklearn.feature_selection import mutual_info_regression, SelectPercentile
from sklearn.impute import SimpleImputer
from sklearn.linear_model import Lasso
from sklearn.metrics import mean_absolute_error, median_absolute_error, mean_squared_error, r2_score
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelBinarizer, StandardScaler, LabelEncoder

from data_processing.data_transformations import split_data_into_subsets, split_dates
from data_processing.feature_constructor import FeatureConstructor as FC
from utils.config import dprint, save_frame


class AutoFITS:

    def __init__(self,
                 schema: dict,
                 frequency: str,
                 method='fits',
                 target=None,
                 learning_algorithm=Lasso,
                 learning_params=dict(),
                 resampling_strategy='sum',
                 imputation_strategy='zero',
                 percentile=80,
                 lag_size=6,
                 cache_data=False,
                 prediction_strategy=None,
                 k=10,
                 test_size=0.1,
                 mov_avg_window=3) -> None:

        self.schema = schema
        self.target = target
        self.learning_algorithm = learning_algorithm
        self.learning_params = learning_params
        self.method = method
        self.frequency = frequency
        self.imputation_strategy = imputation_strategy
        self.resampling_strategy = resampling_strategy
        self.percentile = percentile
        self.lag_size = lag_size
        self.cache_data = cache_data
        self.prediction_strategy = prediction_strategy
        self.k = k
        self.test_size = test_size
        self.mov_avg_window = mov_avg_window

        self.timestamp_column = None
        self.sub_series_column = None
        for key, value in schema.items():
            if value == 'timestamp':
                self.timestamp_column = key
            elif value == 'sub_series_id':
                self.sub_series_column = key

        if not self.timestamp_column:
            warning("Could not find timestamp column in schema!")

        self.feature_model = None
        self.learner = None

        self.mae = None
        self.mse = None
        self.r2 = None
        self.zero_count = None
        self.total_cells = None
        self.data_len = None

        self.standardizing_dict = None
        self.label_encoder = None

        self.cached_subsets = None
        self.cached_y = None
        self.cached_groups = None
        self.cached_x_forecast_resampled = None

        self.meta_features = None

        self.forecast_time_interval = None
        self.x_forecast = None

        self.unique_entities = None
        super().__init__()

    def reset_cached_data(self):
        self.cached_subsets = None
        self.cached_y = None
        self.cached_groups = None
        self.cached_x_forecast_resampled = None

    def process_data(self, X, y, normalize=False):
        if not self.cached_subsets and not self.cached_y:
            X.fillna(X.mode().iloc[0], inplace=True)

            # Cast to respective types
            for column, column_type in self.schema.items():
                if column == self.target:
                    continue
                if (column_type.lower() == 'date' or column_type.lower() == 'timestamp') \
                        and X[column].dtype not in [np.datetime64, np.timedelta64, 'timedelta64[ns]']:
                    X[column] = pd.to_datetime(X[column])
                elif column_type.lower() == 'numeric':
                    X[column] = pd.to_numeric(X[column],
                                              errors='coerce')

            # Ordinal encode sub_series_column (entity id)
            # One-Hot encoding other categorical columns
            df_categorical = X.select_dtypes(include='object')
            if self.sub_series_column and self.sub_series_column in df_categorical.columns:
                le = LabelEncoder()
                le.fit(df_categorical[self.sub_series_column])
                transformed = le.transform(df_categorical[self.sub_series_column])
                X[self.sub_series_column] = pd.Series(transformed,
                                                      index=X.index,
                                                      name=self.sub_series_column)
                df_categorical.drop([self.sub_series_column], axis=1, inplace=True)
                self.label_encoder = le

            for column in df_categorical.columns:
                jobs_encoder = LabelBinarizer()
                jobs_encoder.fit(df_categorical[column])
                transformed = jobs_encoder.transform(df_categorical[column])

                if transformed.shape[1] == 1:
                    col_names = [column + '_' + jobs_encoder.classes_[0]]
                else:
                    col_names = [column + '_' + x for x in jobs_encoder.classes_]
                idxs = X.index
                ohe_df = pd.DataFrame(transformed, columns=col_names, index=idxs)

                X = pd.concat([X, ohe_df], axis=1).drop(column, axis=1)

            # Split dates, but not timestamps
            no_ts_cols = [x for x in X.columns if x != self.timestamp_column]
            new_cols = split_dates(X[no_ts_cols])
            X = X.drop(no_ts_cols, axis=1)
            X[new_cols.columns] = new_cols

            df_subsets, new_y, groups, x_forecast_resampled, x_forecast_group = split_data_into_subsets(X,
                                                                                                        y,
                                                                                                        self.target,
                                                                                                        self.lag_size,
                                                                                                        self.sub_series_column,
                                                                                                        self.timestamp_column,
                                                                                                        self.frequency,
                                                                                                        self.resampling_strategy,
                                                                                                        self.imputation_strategy)

            # Get information for forecasting  next value: observations and corresponding timestamps
            if not self.sub_series_column:
                last_date = df_subsets[-1].tail(1)[self.timestamp_column].iloc[0]
                self.forecast_time_interval = (last_date + 2 * pd.to_timedelta(self.frequency),
                                               last_date + 3 * pd.to_timedelta(self.frequency))
                df_subsets.append(x_forecast_resampled)
                x_forecast_group = pd.concat(x_forecast_group)
                groups.append(x_forecast_group)
            else:
                self.forecast_time_interval = dict()
                for sub in df_subsets:
                    last_date = sub.tail(1)[self.timestamp_column].iloc[0]
                    id = sub[self.sub_series_column].iloc[0]
                    self.forecast_time_interval[id] = (last_date + 2 * pd.to_timedelta(self.frequency),
                                                       last_date + 3 * pd.to_timedelta(self.frequency))
                df_subsets.extend(list(x_forecast_resampled.values()))
                groups.extend(list(x_forecast_group.values()))

            if self.cache_data:
                self.cached_subsets = df_subsets
                self.cached_y = new_y
                self.cached_groups = groups
                self.cached_x_forecast_resampled = x_forecast_resampled
        else:
            df_subsets = self.cached_subsets
            new_y = self.cached_y
            groups = self.cached_groups
            x_forecast_resampled = self.cached_x_forecast_resampled

        if self.method == 'fits':

            targ_feature = pd.DataFrame.from_dict({self.target: new_y})
            targ_feature.reset_index(inplace=True, drop=True)
            new_features = [
                # Uncomment for more features
                # Experimentally, these decreased performance scores

                # FC.get_weekday_peak(groups,
                #                     self.timestamp_column,
                #                     self.target),
                # FC.get_stats_difference_between_observations(groups,
                #                                              self.timestamp_column,
                #                                              self.target,
                #                                              # frequency=self.frequency
                #                                              ),
                # FC.get_stats_difference_between_observations(df_subsets,
                #                                              self.timestamp_column,
                #                                              self.target,
                #                                              # frequency=self.frequency,
                #                                              feature_name='after_resample_no_freq'),
                # FC.get_stats_difference_between_observations(df_subsets,
                #                                              self.timestamp_column,
                #                                              self.target,
                #                                              frequency=self.frequency,
                #                                              feature_name='after_resample'),
                # FC.get_weekend_days_count(groups,
                #                           self.timestamp_column),

                FC.get_stats_difference_between_observations(groups,
                                                             self.timestamp_column,
                                                             self.target,
                                                             frequency=self.frequency,
                                                             # feature_name='div_freq'
                                                             ),
                FC.get_entropy(groups,
                               self.timestamp_column),
                FC.get_entropy(groups,
                               self.target),
                FC.get_entropy(df_subsets,
                               self.target,
                               feature_name='entropy_after_resample'),
                FC.get_relative_dispersion(groups,
                                           self.target),
                FC.get_relative_dispersion(groups,
                                           self.timestamp_column),
                FC.get_relative_dispersion(df_subsets,
                                           self.timestamp_column,
                                           feature_name='relative_dispersion_' + self.timestamp_column +
                                                        '_after_resample'),
                FC.get_relative_dispersion(df_subsets,
                                           self.target,
                                           feature_name='relative_dispersion_' + self.target +
                                                        '_after_resample'),
                FC.get_timestamps_target_mul_avg(df_subsets,
                                                 self.timestamp_column,
                                                 self.target,
                                                 feature_name='timestamps_target_mul_avg_after_resample'),
                FC.get_timestamps_target_mul_avg(groups,
                                                 self.timestamp_column,
                                                 self.target, ),
                FC.get_timestamps_target_avg_diff_mul(groups,
                                                      self.timestamp_column,
                                                      self.target,
                                                      frequency=self.frequency),
                FC.get_timestamps_target_avg_diff_mul(df_subsets,
                                                      self.timestamp_column,
                                                      self.target,
                                                      frequency=self.frequency,
                                                      feature_name='timestamps_target_avg_diff_mul_after_resample'),
                FC.get_min_max_difference_timestamps(groups,
                                                     self.timestamp_column,
                                                     self.target,
                                                     frequency=self.frequency,
                                                     feature_name='min_max_timestamp_difference_div_freq'),
                FC.get_min_max_difference_timestamps(groups,
                                                     self.timestamp_column,
                                                     self.target),
                FC.get_moving_average(df_subsets,
                                      self.target,
                                      self.mov_avg_window),
                FC.get_2d_space_area(df_subsets,
                                     self.timestamp_column,
                                     self.target,
                                     frequency=self.frequency,
                                     feature_name='val_2d_space_area_after_resample'),
                FC.get_2d_space_area(groups,
                                     self.timestamp_column,
                                     self.target,
                                     frequency=self.frequency),
                FC.get_regressive_model_features(df_subsets,
                                                 self.target),
                FC.get_missing_timestamps_count(df_subsets,
                                                groups,
                                                self.timestamp_column),
                targ_feature]

            if self.sub_series_column:
                col = []
                for df in df_subsets:
                    col.append(df[self.sub_series_column].iloc[0])
                sub_series_column = pd.DataFrame.from_dict({self.sub_series_column: col})
                new_features.append(sub_series_column)
        else:
            targ_feature = pd.DataFrame.from_dict({self.target: new_y})
            targ_feature.reset_index(inplace=True, drop=True)
            if self.sub_series_column:
                col = []
                for df in df_subsets:
                    col.append(df[self.sub_series_column].iloc[0])
                sub_series_column = pd.DataFrame.from_dict({self.sub_series_column: col})
                new_features = [targ_feature, sub_series_column]
            else:
                new_features = [targ_feature]

        t_cols = []
        for i in list(reversed(range(self.lag_size))):
            t_cols.append("t-" + str(i))
        cols = t_cols

        df = pd.DataFrame(columns=cols)
        for idx in range(0, len(df_subsets)):
            row = list(df_subsets[idx][self.target].values)
            tmp_df = pd.DataFrame([row], columns=cols)
            df = df.append(tmp_df, ignore_index=True)

        self.zero_count = (df == 0).astype(int).sum(axis=1).sum()
        self.total_cells = df.shape[0] * df.shape[1]

        all_features = [df] + new_features
        df = pd.concat(all_features, axis=1)

        # Split data features into multiple numerical features
        df = split_dates(df)

        # Get data for forecasting next values
        if self.sub_series_column:
            self.x_forecast = df.tail(len(x_forecast_resampled)).copy()
            self.x_forecast.drop(self.target, axis=1)
            df.drop(df.tail(len(x_forecast_resampled)).index,
                    inplace=True)
        else:
            self.x_forecast = df.tail(1).copy()
            self.x_forecast.drop(self.target, axis=1)
            df.drop(df.tail(1).index,
                    inplace=True)

        # Fill missing values
        df = df.dropna(axis=1, how='all')
        fill_NaN = SimpleImputer(strategy='mean')
        df = pd.DataFrame(fill_NaN.fit_transform(df),
                          columns=df.columns,
                          index=df.index)
        self.x_forecast = pd.DataFrame(fill_NaN.transform(self.x_forecast),
                                       columns=self.x_forecast.columns,
                                       index=self.x_forecast.index)

        # Split data
        y = df[self.target]
        X = df.drop(self.target, axis=1)

        # Remove constant columns
        X = X.loc[:, (X != X.iloc[0]).any()]

        # Calculate feature importance
        if self.method != "baseline":
            res = dict(zip(list(X.columns),
                           mutual_info_regression(X, y)
                           ))
            res['frequency'] = self.frequency
            if path.exists("feature_importance.csv"):
                fe_df = pd.read_csv("feature_importance.csv")
                new_entry = pd.DataFrame(res, index=[0])
                fe_df = pd.concat([fe_df, new_entry], ignore_index=True)
                save_frame(fe_df,
                           "feature_importance.csv")
            else:
                fe_df = pd.DataFrame(res, index=[0])
                save_frame(fe_df,
                           "feature_importance.csv")

        # Save dataset
        saved_x = X.copy()
        saved_x[self.target] = y.copy()
        save_frame(saved_x,
                   self.method + '_' + self.frequency + '.csv')

        # Store feature importance
        try:
            res = dict(zip(list(X.columns),
                           mutual_info_regression(X, y)
                           ))
            res['frequency'] = self.frequency
            if path.exists("feature_importance_fits.csv"):
                fe_df = pd.read_csv("feature_importance_fits.csv")
                new_entry = pd.DataFrame(res, index=[0])
                fe_df = pd.concat([fe_df, new_entry], ignore_index=True)
                fe_df.to_csv("feature_importance_fits.csv", index=False)
            else:
                fe_df = pd.DataFrame(res, index=[0])
                fe_df.to_csv("feature_importance_fits.csv", index=False)
        except Exception:
            pass

        # Select features in the top percentile
        if self.method != 'baseline' and self.percentile != 100:
            selection = SelectPercentile(mutual_info_regression, percentile=self.percentile)
            if self.sub_series_column:
                col = X[self.sub_series_column]
                X = X.drop(self.sub_series_column, axis=1)
            x_features = selection.fit_transform(X, y)
            columns = np.asarray(X.columns.values)
            support = np.asarray(selection.get_support())
            columns_with_support = columns[support]
            X = pd.DataFrame(x_features,
                             columns=columns_with_support,
                             index=X.index)
            if self.sub_series_column and self.sub_series_column not in X.columns:
                X[self.sub_series_column] = col
            self.x_forecast = self.x_forecast[X.columns]
            dprint("New shape after feature selection: ", X.shape)
            dprint("   => Features: ", X.columns)

        # Normalize groups of observations according to the sub_series_column attribute
        if normalize:
            if self.sub_series_column:
                X[self.target] = y
                cols = list(X.columns)
                cols.remove(self.sub_series_column)

                self.standardizing_dict = dict()
                gb = X.groupby(self.sub_series_column)
                df_list = [gb.get_group(x) for x in gb.groups]
                new_df_list = []
                std_x_forecast = pd.DataFrame(columns=self.x_forecast.columns)
                for d in df_list:
                    sc = StandardScaler()
                    data = d.copy()
                    subset_data = d[cols]
                    transformed_data = sc.fit_transform(subset_data)
                    idxs = d.index
                    data[cols] = pd.DataFrame(transformed_data,
                                              columns=cols,
                                              index=idxs)
                    new_df_list.append(data)
                    sub_series_id = data[self.sub_series_column].iloc[0]
                    self.standardizing_dict[sub_series_id] = sc

                    data = self.x_forecast[self.x_forecast[self.sub_series_column] == sub_series_id].copy()
                    data[self.target] = [1] * len(data)
                    subset_data = data[cols]
                    transformed_data = sc.transform(subset_data)
                    idxs = data.index
                    data[cols] = pd.DataFrame(transformed_data,
                                              columns=cols,
                                              index=idxs)
                    data = data.drop(self.target, axis=1)
                    std_x_forecast = pd.concat([std_x_forecast, data])

                X = pd.concat(new_df_list)
                self.x_forecast = std_x_forecast
            else:
                X[self.target] = y
                sc = StandardScaler()
                X = pd.DataFrame(sc.fit_transform(X),
                                 columns=X.columns,
                                 index=X.index)
                self.x_forecast = pd.DataFrame(sc.transform(self.x_forecast),
                                               columns=self.x_forecast.columns,
                                               index=self.x_forecast.index)
                self.standardizing_dict = sc

            y = X[self.target]
            X = X.drop(self.target, axis=1)

        if self.sub_series_column:
            self.unique_entities = X[self.sub_series_column].unique()

        self.data_len = len(X)
        return X, y

    def fit(self, X, y):
        if not self.target:
            self.target = X.columns[-1]

        pr_X, pr_y = self.process_data(X, y)

        if not self.sub_series_column:
            if self.test_size < 1:
                X_train, X_test, y_train, y_test = train_test_split(
                    pr_X, pr_y,
                    test_size=self.test_size,
                    shuffle=False,
                    random_state=42)
            else:
                X_test = pr_X.tail(int(self.test_size))
                X_train = pr_X.drop(pr_X.tail(int(self.test_size)).index)
                y_train = pr_y[X_train.index]
                y_test = pr_y[X_test.index]
        else:
            gb = pr_X.groupby(self.sub_series_column)
            df_list = [gb.get_group(x) for x in gb.groups]
            X_test = pd.DataFrame(columns=pr_X.columns)
            X_train = pd.DataFrame(columns=pr_X.columns)
            y_train = pd.Series(name=pr_y.name, dtype=np.float64)
            y_test = pd.Series(name=pr_y.name, dtype=np.float64)
            for i in range(len(df_list)):
                df = df_list[i]
                if self.test_size < 1:
                    t_size = int(self.test_size * len(df))
                    if t_size == 0 and len(X) > 1:
                        t_size = 1
                elif len(df) < self.test_size:
                    t_size = len(df)
                else:
                    t_size = self.test_size
                x_to_test = df.tail(t_size)
                x_to_train = df.drop(df.tail(t_size).index)
                X_train = X_train.append(x_to_train)
                X_test = X_test.append(x_to_test)
                y_train = y_train.append(pr_y[x_to_train.index])
                y_test = y_test.append(pr_y[x_to_test.index])

        save_frame(X_train,
                   self.method + ".csv")

        if not self.prediction_strategy:
            self._fit_learner(X_train,
                              y_train)
            yh_test = self.learner.predict(X_test)
        else:
            self._fit_learner_ensemble(X_train,
                                       y_train)
            yh_test = self._predict_ensemble(X_test)

        save_frame(X_test,
                   self.method + "_TEST_" + self.frequency + ".csv")
        if self.standardizing_dict:
            if self.sub_series_column:
                y_idx = 0
                for index, row in X_test.iterrows():
                    r1 = row.copy()

                    r1[self.target] = yh_test[y_idx]
                    r1 = r1.drop(labels=self.sub_series_column)
                    sc = self.standardizing_dict[row[self.sub_series_column]]
                    transformed = pd.Series(sc.inverse_transform(r1),
                                            index=r1.index)
                    yh_test[y_idx] = transformed[self.target]

                    r1[self.target] = y_test.iloc[y_idx]
                    transformed = pd.Series(sc.inverse_transform(r1),
                                            index=r1.index)
                    y_test.iloc[y_idx] = transformed[self.target]
                    y_idx += 1
            else:
                sc = self.standardizing_dict
                yh_test = pd.Series(sc.inverse_transform(yh_test),
                                    index=yh_test.index)

        self.mae = mean_absolute_error(y_test, yh_test)
        self.mse = mean_squared_error(y_test, yh_test)
        self.r2 = r2_score(y_test, yh_test)

        print("R2: ", self.r2)
        print("MAE: ", self.mae)
        print("Median AE: ", median_absolute_error(y_test, yh_test))

        return self

    def _fit_learner(self,
                     X, y):
        pred_model = self.learning_algorithm(**self.learning_params)
        pred_model.fit(X, y)
        self.learner = pred_model
        return pred_model

    def _fit_learner_ensemble(self,
                              X, y):
        ensemble = dict()
        mfs = pd.DataFrame()

        # Calculate each sub-series meta-features and create models for ensemble
        for id in X[self.sub_series_column].unique():
            id_x_train = X[X[self.sub_series_column] == id]
            id_y_train = y[id_x_train.index]

            try:
                metafeatures = Metafeatures()
                id_mfs = metafeatures.compute(id_x_train, id_y_train)
                id_mfs[self.sub_series_column] = id
                if mfs.empty:
                    mfs = pd.DataFrame.from_dict(id_mfs)
                else:
                    mfs = pd.concat([mfs, pd.DataFrame.from_dict(id_mfs)], ignore_index=True)
            except Exception:
                pass

            model = self.learning_algorithm(**self.learning_params)
            model.fit(id_x_train, id_y_train)
            ensemble[id] = model
        mfs.set_index([self.sub_series_column])

        # Encode categorical values in metafeatures
        df_categorical = mfs.select_dtypes(include='object')
        for c_col in df_categorical.columns:
            le = LabelEncoder()
            le.fit(mfs[c_col])
            transformed = le.transform(mfs[c_col])
            mfs[c_col] = transformed

        # Missing values imputation
        fill_NaN = SimpleImputer(strategy='mean')
        mfs = mfs.replace([np.inf, -np.inf], np.nan)
        sub_series_column = mfs[self.sub_series_column].copy()
        mfs = pd.DataFrame(StandardScaler().fit_transform(mfs), columns=mfs.columns, index=mfs.index)
        mfs[self.sub_series_column] = sub_series_column
        mfs = mfs.dropna(axis=1, how='all')
        self.meta_features = pd.DataFrame(fill_NaN.fit_transform(mfs), columns=mfs.columns, index=mfs.index)
        self.learner = ensemble

    def _predict(self,
                 X):
        return self.learner.predict(X)

    def _predict_ensemble(self,
                          X):
        ensemble = self.learner
        mfs = self.meta_features
        k = self.k

        predictions = []
        for idx, r in X.iterrows():
            id = r[self.sub_series_column]
            row = r.values.reshape(1, -1)

            # Predict using the id's model in the ensemble
            p0 = None
            if id in ensemble:
                p0 = ensemble[id].predict(row)[0]

            # Calculate similarity
            pn = None
            if id in mfs:
                id_mfs = mfs.loc[id].values.reshape(1, -1)
                pn = []
                for key, model in ensemble.items():
                    if key in mfs.index:
                        p = model.predict(row)[0]
                        weight = cosine_similarity(mfs.loc[key].values.reshape(1, -1), id_mfs)
                        pn.append((p, weight))

                pn = sorted(pn, key=lambda t: t[1], reverse=True)[:k]

            # Calculate prediction based on weights
            if p0:
                if pn:
                    pn.append([(p0, 1)])
                    prediction = sum([p * w for p, w in pn]) / sum([w for _, w in pn])
                else:
                    pn = []
                    for model in ensemble.values():
                        pn.append(model.predict(row)[0])
                    prediction = np.mean(pn) * 0.2 + p0 * 0.8
            elif pn:
                prediction = sum([p * w for p, w in pn]) / sum([w for _, w in pn])
            else:
                pn = []
                for model in ensemble.values():
                    pn.append(model.predict(row)[0])
                prediction = np.mean(pn)
            predictions.append(prediction)

        return predictions

    def forecast(self,
                 ids=None):
        if not self.learner:
            raise Exception("Fit a model before making any predictions.")

        predict_func = self._predict_ensemble if self.prediction_strategy else self._predict

        if self.sub_series_column:
            if not ids:
                ids = self.x_forecast[self.sub_series_column].unique()
            else:
                ids = self.label_encoder.transform(ids)
            forecasts = dict()
            for s_id in ids:
                data = self.x_forecast[self.x_forecast[self.sub_series_column] == s_id]
                p = predict_func(data)
                forecasts[s_id] = p
                if self.standardizing_dict:
                    d = data.drop(self.sub_series_column,
                                  axis=1)
                    d[self.target] = p
                    sc = self.standardizing_dict[s_id]
                    transformed = pd.DataFrame(sc.inverse_transform(d),
                                               columns=d.columns,
                                               index=d.index)
                    p = transformed[self.target].values
                if self.label_encoder:
                    l = self.label_encoder.inverse_transform([int(s_id)])[0]
                else:
                    l = s_id
                print("Forecast id ", l, ": ", p)
                print("Time window: ", self.forecast_time_interval[s_id], "\n")
            return forecasts
        else:
            forecast = predict_func(self.x_forecast)
            print("Forecast : ", forecast)
            print("Time window: ", self.forecast_time_interval)
            return forecast
