from logging import warning

import numpy as np
import pandas as pd
from metalearn import Metafeatures
from sklearn.impute import SimpleImputer
from sklearn.linear_model import Lasso
from sklearn.metrics import mean_absolute_error, mean_squared_error, r2_score, median_absolute_error
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder, StandardScaler
from vest.config.aggregation_functions import SUMMARY_OPERATIONS_SMALL
from vest.models.univariate import UnivariateVEST

from data_processing.data_transformations import split_data_into_subsets
from utils.config import save_frame


class VESTForecaster:

    def __init__(self,
                 schema=None,
                 target=None,
                 frequency=None,
                 learning_params=dict(),
                 resampling_strategy='sum',
                 imputation_strategy='zero',
                 lag_size=6,
                 learning_algorithm=Lasso,
                 test_size=0.1,
                 prediction_strategy=None,
                 k=10) -> None:

        self.schema = schema
        self.target = target
        self.frequency = frequency
        self.imputation_strategy = imputation_strategy
        self.resampling_strategy = resampling_strategy
        self.learning_params = learning_params
        self.lag_size = lag_size
        self.learning_algorithm = learning_algorithm
        self.test_size = test_size
        self.prediction_strategy = prediction_strategy
        self.k = k

        self.timestamp_column = None
        self.sub_series_column = None
        if schema:
            for key, value in schema.items():
                if value == 'timestamp':
                    self.timestamp_column = key
                elif value == 'sub_series_id':
                    self.sub_series_column = key

            if not self.timestamp_column:
                warning("Could not find timestamp column in schema!")

        self.mae = None
        self.mse = None
        self.r2 = None
        self.zero_count = None
        self.total_cells = None
        self.data_len = None
        self.x_forecast = None
        self.forecast_time_interval = None

        if not self.sub_series_column:
            self.feature_model = None
        else:
            self.feature_model = dict()

        self.leaner = None
        self.standardizing_dict = None

        self.label_encoder = None

        super().__init__()

    def process_data(self, data):
        data.fillna(data.mode().iloc[0], inplace=True)

        # Cast to respective types
        if self.schema:
            for column, column_type in self.schema.items():
                if column == self.target:
                    continue
                if (column_type.lower() == 'date' or column_type.lower() == 'timestamp') \
                        and data[column].dtype not in [np.datetime64, np.timedelta64, 'timedelta64[ns]']:
                    data[column] = pd.to_datetime(data[column])
                elif column_type.lower() == 'numeric':
                    data[column] = pd.to_numeric(data[column],
                                                 errors='coerce')
        # Label encode ids:
        df_categorical = data.select_dtypes(include='object')
        if self.sub_series_column and self.sub_series_column in df_categorical.columns:
            le = LabelEncoder()
            le.fit(df_categorical[self.sub_series_column])
            transformed = le.transform(df_categorical[self.sub_series_column])
            data[self.sub_series_column] = pd.Series(transformed,
                                                     index=data.index,
                                                     name=self.sub_series_column)
            df_categorical.drop([self.sub_series_column], axis=1, inplace=True)
            self.label_encoder = le
            data = data.sort_values(by=[self.sub_series_column])

        if self.frequency:
            y = data[self.target]
            X = data.drop(self.target, axis=1)
            df_subsets, new_y, _, x_forecast_resampled, _ = split_data_into_subsets(X,
                                                                                    y,
                                                                                    self.target,
                                                                                    self.lag_size,
                                                                                    self.sub_series_column,
                                                                                    self.timestamp_column,
                                                                                    self.frequency,
                                                                                    self.resampling_strategy,
                                                                                    self.imputation_strategy)

            if not self.sub_series_column:
                last_date = df_subsets[-1].tail(1)[self.timestamp_column].iloc[0]
                self.forecast_time_interval = (last_date + 2 * pd.to_timedelta(self.frequency),
                                               last_date + 3 * pd.to_timedelta(self.frequency))
                df_subsets.append(x_forecast_resampled)
            else:
                self.forecast_time_interval = dict()
                for sub in df_subsets:
                    last_date = sub.tail(1)[self.timestamp_column].iloc[0]
                    id = sub[self.sub_series_column].iloc[0]
                    self.forecast_time_interval[id] = (last_date + 2 * pd.to_timedelta(self.frequency),
                                                       last_date + 3 * pd.to_timedelta(self.frequency))
                df_subsets.extend(list(x_forecast_resampled.values()))

            t_cols = []
            for i in list(reversed(range(self.lag_size))):
                t_cols.append("t-" + str(i))
            cols = t_cols

            if self.sub_series_column:
                cols.append(self.sub_series_column)

            data = pd.DataFrame(columns=cols)
            for idx in range(0, len(df_subsets)):
                row = list(df_subsets[idx][self.target].values)
                if self.sub_series_column:
                    row.append(df_subsets[idx][self.sub_series_column].iloc[0])
                tmp_df = pd.DataFrame([row], columns=cols)
                data = data.append(tmp_df, ignore_index=True)

            if self.sub_series_column:
                x_forecast = data.tail(len(x_forecast_resampled))
                data.drop(data.tail(len(x_forecast_resampled)).index,
                          inplace=True)
            else:
                x_forecast = data.tail(1)
                data.drop(data.tail(1).index,
                          inplace=True)

            if not self.sub_series_column:
                y = new_y
                X = data

                self.zero_count = (X == 0).astype(int).sum(axis=1).sum()
                self.total_cells = X.shape[0] * X.shape[1]

                if self.test_size < 1:
                    X_train, X_test, y_train, y_test = train_test_split(
                        X, y,
                        test_size=self.test_size,
                        shuffle=False,
                        random_state=42)
                else:
                    X_test = X.tail(int(self.test_size))
                    X_train = X.drop(X.tail(int(self.test_size)).index)
                    y_train = y[X_train.index]
                    y_test = y[X_test.index]

                X_train = self._training_features(X_train)
                X_test = self._testing_features(X_test)
                x_forecast = self._testing_features(x_forecast)

                # Get data for forecasting next values
                self.x_forecast = x_forecast

                return X_train, X_test, y_train, y_test
            else:
                self.zero_count = (data == 0).astype(int).sum(axis=1).sum()
                self.total_cells = data.shape[0] * data.shape[1]

                data[self.target] = new_y
                gb = data.groupby(self.sub_series_column)
                df_list = [gb.get_group(x) for x in gb.groups]

                X_train = []
                X_test = []
                y_train = []
                y_test = []

                test = pd.DataFrame()
                for df in df_list:
                    s_id = df[self.sub_series_column].iloc[0]

                    y = df[self.target]
                    X = df.drop([self.target, self.sub_series_column], axis=1)

                    X = X.astype('float')
                    y = y.astype('float')

                    X_tst = pd.DataFrame(columns=X.columns)
                    X_trn = pd.DataFrame(columns=X.columns)
                    y_trn = pd.Series(name=self.target, dtype=np.float64)
                    y_tst = pd.Series(name=self.target, dtype=np.float64)

                    if self.test_size < 1:
                        t_size = int(self.test_size * len(X))
                        if t_size == 0 and len(X) > 1:
                            t_size = 1
                    elif len(X) < self.test_size:
                        t_size = len(X)
                    else:
                        t_size = self.test_size
                    x_to_test = X.tail(t_size)
                    x_to_train = X.drop(X.tail(t_size).index)
                    X_trn = X_trn.append(x_to_train)
                    X_tst = X_tst.append(x_to_test)
                    y_trn = y_trn.append(y[x_to_train.index])
                    y_tst = y_tst.append(y[x_to_test.index])

                    X_tr_augmented = self._training_features(X_trn,
                                                             s_id)
                    if len(X_tst) > 0:
                        X_ts_augmented = self._testing_features(X_tst,
                                                                s_id)
                    else:
                        X_ts_augmented = pd.DataFrame(columns=X_tr_augmented.columns)

                    X_tr_augmented[self.sub_series_column] = s_id
                    X_ts_augmented[self.sub_series_column] = s_id

                    X_train.append(X_tr_augmented)
                    X_test.append(X_ts_augmented)
                    y_train.append(y_trn)
                    y_test.append(y_tst)
                    test = pd.concat([test, X_ts_augmented], ignore_index=True)

                X_train = pd.concat(X_train)
                X_test = pd.concat(X_test)
                y_train = pd.concat(y_train)
                y_test = pd.concat(y_test)

                x_forecast = x_forecast.astype('float')
                x_forecast_aug = []
                for s_id in x_forecast[self.sub_series_column].unique():
                    try:
                        d = x_forecast[x_forecast[self.sub_series_column] == s_id]
                        d = d.drop(self.sub_series_column, axis=1)
                        X_ts_augmented = self._testing_features(d,
                                                                s_id)
                        X_ts_augmented[self.sub_series_column] = s_id
                        x_forecast_aug.append(X_ts_augmented)
                    except Exception as e:
                        print("Can not obtain forecast for id ", s_id)
                        print("    => ", str(e))
                x_forecast = pd.concat(x_forecast_aug)

                fill_NaN = SimpleImputer(strategy='mean')
                X_train = pd.DataFrame(fill_NaN.fit_transform(X_train), columns=X_train.columns, index=X_train.index)
                X_test = pd.DataFrame(fill_NaN.transform(X_test), columns=X_test.columns, index=X_test.index)
                x_forecast = pd.DataFrame(fill_NaN.transform(x_forecast), columns=x_forecast.columns,
                                          index=x_forecast.index)

                # Get data for forecasting next values
                self.x_forecast = x_forecast

                return X_train, X_test, y_train, y_test
        return data

    def _training_features(self,
                           X,
                           s_id=None):
        model = UnivariateVEST()
        model.fit(X=X.values,
                  correlation_thr=0.9,
                  apply_transform_operators=True,
                  summary_operators=SUMMARY_OPERATIONS_SMALL)

        training_features = model.dynamics
        idx = X.index
        X.reset_index(inplace=True,
                      drop=True)
        X_augmented = pd.concat([X, training_features],
                                axis=1)
        X_augmented.set_index(idx,
                              inplace=True)
        if not s_id and s_id != 0:
            self.feature_model = model
        else:
            X_augmented[self.sub_series_column] = s_id
            self.feature_model[s_id] = model

        return X_augmented

    def _testing_features(self,
                          X,
                          s_id=None):
        if not s_id and s_id != 0:
            testing_features = self.feature_model.transform(X.values)
        else:
            testing_features = self.feature_model[s_id].transform(X.values)
        testing_features.index = X.index
        X_ts_augmented = pd.concat([X, testing_features], axis=1)
        return X_ts_augmented

    def fit(self, data):
        if not self.target:
            self.target = data.columns[-1]

        X_train, X_test, y_train, y_test = self.process_data(data)

        if not self.sub_series_column:
            self._fit_learner(X_train,
                              y_train)
            yh_test = self._predict(X_test)

            X_train_copy = X_train.copy()
            X_train_copy[self.target] = y_train.copy()
            X_test[self.target] = y_test.copy()
            to_save = pd.concat([X_train_copy, X_test], ignore_index=True)
            save_frame(to_save,
                       'VEST_' + self.frequency + '.csv')

            self.mae = mean_absolute_error(y_test, yh_test)
            self.mse = mean_squared_error(y_test, yh_test)
            self.r2 = r2_score(y_test, yh_test)

            print("R2: ", self.r2)
            print("MAE: ", self.mae)
            print("Median AE: ", median_absolute_error(y_test, yh_test))

        else:
            if not self.prediction_strategy:
                self._fit_learner(X_train,
                                  y_train)
                yh_test = self._predict(X_test)

                X_test_copy = X_test.copy()
                X_test_copy[self.target] = y_train.copy()

                to_save = pd.concat([X_train, X_test_copy], ignore_index=True)

                save_frame(to_save,
                           'VEST_' + self.frequency + '.csv')

                save_frame(X_test,
                           "VEST_TEST_" + self.frequency + ".csv")
            else:
                X_test_copy = X_test.copy()
                X_test_copy[self.target] = y_train.copy()
                to_save = pd.concat([X_train, X_test_copy], ignore_index=True)
                save_frame(to_save,
                           'VEST_' + self.frequency + '.csv')

                self._fit_learner_ensemble(X_train,
                                           y_train)
                yh_test = self._predict_ensemble(X_test)

            self.mae = mean_absolute_error(y_test, yh_test)
            self.mse = mean_squared_error(y_test, yh_test)
            self.r2 = r2_score(y_test, yh_test)

            print("R2: ", self.r2)
            print("MAE: ", self.mae)
            print("Median AE: ", median_absolute_error(y_test, yh_test))
        return self

    def _fit_learner(self,
                     X, y):
        pred_model = self.learning_algorithm(**self.learning_params)
        pred_model.fit(X, y)
        self.learner = pred_model
        return pred_model

    def _fit_learner_ensemble(self,
                              X, y):
        ensemble = dict()
        mfs = pd.DataFrame()

        # Calculate each sub-series meta-features and create models for ensemble
        for id in X[self.sub_series_column].unique():
            id_x_train = X[X[self.sub_series_column] == id]
            id_y_train = y[id_x_train.index]

            try:
                metafeatures = Metafeatures()
                id_mfs = metafeatures.compute(id_x_train, id_y_train)
                id_mfs[self.sub_series_column] = id
                if mfs.empty:
                    mfs = pd.DataFrame.from_dict(id_mfs)
                else:
                    mfs = pd.concat([mfs, pd.DataFrame.from_dict(id_mfs)], ignore_index=True)
            except Exception:
                pass

            model = self.learning_algorithm(**self.learning_params)
            model.fit(id_x_train, id_y_train)
            ensemble[id] = model
        mfs.set_index([self.sub_series_column])

        # Encode categorical values in metafeatures
        df_categorical = mfs.select_dtypes(include='object')
        for c_col in df_categorical.columns:
            le = LabelEncoder()
            le.fit(mfs[c_col])
            transformed = le.transform(mfs[c_col])
            mfs[c_col] = transformed

        # Missing values imputation
        fill_NaN = SimpleImputer(strategy='mean')
        mfs = mfs.replace([np.inf, -np.inf], np.nan)
        sub_series_column = mfs[self.sub_series_column].copy()
        mfs = pd.DataFrame(StandardScaler().fit_transform(mfs), columns=mfs.columns, index=mfs.index)
        mfs[self.sub_series_column] = sub_series_column
        mfs = mfs.dropna(axis=1, how='all')
        self.meta_features = pd.DataFrame(fill_NaN.fit_transform(mfs), columns=mfs.columns, index=mfs.index)
        self.learner = ensemble

    def _predict(self,
                 X):
        return self.learner.predict(X)

    def _predict_ensemble(self,
                          X):
        ensemble = self.learner
        mfs = self.meta_features
        k = self.k

        predictions = []
        for idx, r in X.iterrows():
            id = r[self.sub_series_column]
            row = r.values.reshape(1, -1)

            # Predict using the id's model in the ensemble
            p0 = None
            if id in ensemble:
                p0 = ensemble[id].predict(row)[0]

            # Calculate similarity
            pn = None
            if id in mfs:
                id_mfs = mfs.loc[id].values.reshape(1, -1)
                pn = []
                for key, model in ensemble.items():
                    if key in mfs.index:
                        p = model.predict(row)[0]
                        weight = cosine_similarity(mfs.loc[key].values.reshape(1, -1), id_mfs)
                        pn.append((p, weight))

                pn = sorted(pn, key=lambda t: t[1], reverse=True)[:k]

            # Calculate prediction based on weights
            if p0:
                if pn:
                    pn.append([(p0, 1)])
                    prediction = sum([p * w for p, w in pn]) / sum([w for _, w in pn])
                else:
                    pn = []
                    for model in ensemble.values():
                        pn.append(model.predict(row)[0])
                    prediction = np.mean(pn) * 0.2 + p0 * 0.8
            elif pn:
                prediction = sum([p * w for p, w in pn]) / sum([w for _, w in pn])
            else:
                pn = []
                for model in ensemble.values():
                    pn.append(model.predict(row)[0])
                prediction = np.mean(pn)
            predictions.append(prediction)

        return predictions

    def forecast(self,
                 ids=None):
        if not self.learner:
            raise Exception("Fit a model before making any predictions.")

        predict_func = self._predict_ensemble if self.prediction_strategy else self._predict

        if self.sub_series_column:
            if not ids:
                ids = self.x_forecast[self.sub_series_column].unique()
            else:
                ids = self.label_encoder.transform(ids)
            forecasts = dict()
            for s_id in ids:
                data = self.x_forecast[self.x_forecast[self.sub_series_column] == s_id]
                p = predict_func(data)
                forecasts[s_id] = p
                if self.standardizing_dict:
                    d = data.drop(self.sub_series_column,
                                  axis=1)
                    d[self.target] = p
                    sc = self.standardizing_dict[s_id]
                    transformed = pd.DataFrame(sc.inverse_transform(d),
                                               columns=d.columns,
                                               index=d.index)
                    p = transformed[self.target].values
                if self.label_encoder:
                    l = self.label_encoder.inverse_transform([int(s_id)])[0]
                else:
                    l = s_id
                print("Forecast id ", l, ": ", p)
                print("Time window: ", self.forecast_time_interval[s_id], "\n")
            return forecasts
        else:
            forecast = predict_func(self.x_forecast)
            print("Forecast : ", forecast)
            print("Time window: ", self.forecast_time_interval)
            return forecast
