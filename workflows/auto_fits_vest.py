from logging import warning
from os import path

import numpy as np
import pandas as pd
from metalearn import Metafeatures
from sklearn.feature_selection import mutual_info_regression, SelectPercentile
from sklearn.impute import SimpleImputer
from sklearn.linear_model import Lasso
from sklearn.metrics import mean_absolute_error, median_absolute_error, mean_squared_error, r2_score
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.preprocessing import LabelEncoder, StandardScaler

from utils.config import save_frame
from workflows.auto_fits import AutoFITS
from workflows.vest import VESTForecaster


class AutoFV:

    def __init__(self,
                 schema: dict,
                 target=None,
                 learning_algorithm=Lasso,
                 learning_params=dict(),
                 frequency='auto',
                 aggregating_strategy='sum',
                 imputation_strategy='zero',
                 percentile=100,
                 lag_size=6,
                 cache_data=False,
                 prediction_strategy=None,
                 test_size=0.1,
                 k=10,
                 mov_avg_window=3) -> None:

        self.schema = schema
        self.target = target
        self.learning_algorithm = learning_algorithm
        self.learning_params = learning_params
        self.frequency = frequency
        self.imputation_strategy = imputation_strategy
        self.aggregating_strategy = aggregating_strategy
        self.percentile = percentile
        self.lag_size = lag_size
        self.cache_data = cache_data
        self.prediction_strategy = prediction_strategy
        self.test_size = test_size
        self.k = k
        self.mov_avg_window = mov_avg_window

        self.timestamp_column = None
        self.sub_series_column = None
        for key, value in schema.items():
            if value == 'timestamp':
                self.timestamp_column = key
            elif value == 'sub_series_id':
                self.sub_series_column = key

        if not self.timestamp_column:
            warning("Could not find timestamp column in schema!")

        self.feature_model = None
        self.leaner = None

        self.mae = None
        self.mse = None
        self.r2 = None
        self.zero_count = None
        self.total_cells = None
        self.data_len = None

        self.standardizing_dict = None

        self.cached_subsets = None
        self.cached_y = None
        self.cached_groups = None
        self.label_encoder = None

        self.x_forecast = None
        self.forecast_time_interval = None
        super().__init__()

    def process_data(self, X, y):

        fits = AutoFITS(self.schema,
                        method='fits',
                        target=self.target,
                        learning_algorithm=self.learning_algorithm,
                        learning_params=self.learning_params,
                        frequency=self.frequency,
                        resampling_strategy=self.aggregating_strategy,
                        imputation_strategy=self.imputation_strategy,
                        percentile=99,
                        lag_size=self.lag_size,
                        cache_data=self.cache_data,
                        prediction_strategy=self.prediction_strategy,
                        mov_avg_window=self.mov_avg_window)

        vest = VESTForecaster(self.schema,
                              target=self.target,
                              learning_algorithm=self.learning_algorithm,
                              learning_params=self.learning_params,
                              frequency=self.frequency,
                              resampling_strategy=self.aggregating_strategy,
                              imputation_strategy=self.imputation_strategy,
                              lag_size=self.lag_size)

        fits_X, _ = fits.process_data(X.copy(), y.copy(), normalize=False)

        fits_col_names = ['FITS_' + x if not x.startswith('t-') else x for x in fits_X.columns]
        fits_X.rename(columns=dict(zip(fits_X.columns, fits_col_names)),
                      inplace=True)

        X[self.target] = y.copy()

        self.label_encoder = fits.label_encoder

        lag_columns = ["t-" + str(i) for i in list(reversed(range(self.lag_size)))]
        lag_columns[-1] = "t-0"

        vest_X_train, vest_X_test, y_train, y_test = vest.process_data(X)

        vest_col_names = ['VEST_' + x if not x.startswith('t-') else x for x in vest_X_train.columns]
        vest_X_train.rename(columns=dict(zip(vest_X_train.columns, vest_col_names)),
                            inplace=True)
        vest_X_test.rename(columns=dict(zip(vest_X_test.columns, vest_col_names)),
                           inplace=True)

        fits_features = [f for f in fits_X.columns if f not in lag_columns]
        fits_X = fits_X[fits_features]

        fits_X_train = fits_X.loc[vest_X_train.index]
        fits_X_test = fits_X.loc[vest_X_test.index]

        X_train = pd.concat([vest_X_train, fits_X_train], axis=1)
        X_test = pd.concat([vest_X_test, fits_X_test], axis=1)

        # Filter sub series column
        if self.sub_series_column:
            X_train = X_train.rename(columns={"VEST_" + self.sub_series_column: self.sub_series_column})
            X_test = X_test.rename(columns={"VEST_" + self.sub_series_column: self.sub_series_column})
            X_train = X_train.drop("FITS_" + self.sub_series_column,
                                   axis=1)
            X_test = X_test.drop("FITS_" + self.sub_series_column,
                                 axis=1)

        # Save dataset
        X_test_copy = X_test.copy()
        X_test_copy[self.target] = y_test.copy()
        X_train_copy = X_train.copy()
        X_train_copy[self.target] = y_train.copy()
        to_save = pd.concat([X_train_copy, X_test_copy], ignore_index=True)
        save_frame(to_save,
                   'FITS_VEST_' + self.frequency + '.csv')

        self.forecast_time_interval = vest.forecast_time_interval
        if self.sub_series_column:
            for k, v in fits.forecast_time_interval.items():
                self.forecast_time_interval[k] = v

        vest_X_forecast = vest.x_forecast.rename(columns=dict(zip(vest.x_forecast.columns, vest_col_names)))
        fits_X_forecast = fits.x_forecast.rename(columns=dict(zip(fits.x_forecast.columns, fits_col_names)))

        fits_X_forecast = fits_X_forecast[fits_features]
        if self.sub_series_column:
            vest_X_forecast = vest_X_forecast.rename(columns={"VEST_" + self.sub_series_column: self.sub_series_column})
            fits_X_forecast = fits_X_forecast.rename(columns={"FITS_" + self.sub_series_column: self.sub_series_column})
            self.x_forecast = vest_X_forecast.merge(fits_X_forecast,
                                                    how='left',
                                                    on=self.sub_series_column)
        else:
            self.x_forecast = pd.concat([vest_X_forecast, fits_X_forecast], axis=1)
        try:
            res = dict(zip(list(X_train.columns),
                           mutual_info_regression(X_train, y_train)
                           ))
            res['frequency'] = self.frequency
            if path.exists("feature_importance_vf.csv"):
                fe_df = pd.read_csv("feature_importance_vf.csv")
                new_entry = pd.DataFrame(res, index=[0])
                fe_df = pd.concat([fe_df, new_entry], ignore_index=True)
                fe_df.to_csv("feature_importance_vf.csv", index=False)
            else:
                fe_df = pd.DataFrame(res, index=[0])
                fe_df.to_csv("feature_importance_vf.csv", index=False)
        except Exception:
            pass

        if self.sub_series_column:
            col = X_train[self.sub_series_column]
            X_train = X_train.drop(self.sub_series_column, axis=1)

        selection = SelectPercentile(mutual_info_regression, percentile=self.percentile)
        x_features = selection.fit_transform(X_train, y_train)

        columns = np.asarray(X_train.columns.values)
        support = np.asarray(selection.get_support())
        columns_with_support = columns[support]
        X_train = pd.DataFrame(x_features,
                               columns=columns_with_support,
                               index=X_train.index)

        if self.sub_series_column:
            X_train[self.sub_series_column] = col

        X_test = X_test[X_train.columns]
        self.x_forecast = self.x_forecast[X_train.columns]

        return X_train, X_test, y_train, y_test

    def fit(self, X, y):
        if not self.target:
            self.target = X.columns[-1]

        X_train, X_test, y_train, y_test = self.process_data(X, y)

        if not self.sub_series_column:
            self._fit_learner(X_train,
                              y_train)
            yh_test = self._predict(X_test)

            X_train_copy = X_train.copy()
            X_train_copy[self.target] = y_train.copy()
            X_test[self.target] = y_test.copy()
            to_save = pd.concat([X_train_copy, X_test], ignore_index=True)

        else:
            if not self.prediction_strategy:
                self._fit_learner(X_train,
                                  y_train)
                yh_test = self._predict(X_test)

                X_test_copy = X_test.copy()
                X_test_copy[self.target] = y_train.copy()

                to_save = pd.concat([X_train, X_test_copy], ignore_index=True)

            else:
                X_test_copy = X_test.copy()
                X_test_copy[self.target] = y_train.copy()
                to_save = pd.concat([X_train, X_test_copy], ignore_index=True)

                self._fit_learner_ensemble(X_train,
                                           y_train)
                yh_test = self._predict_ensemble(X_test)

        save_frame(to_save,
                   'FITS_VEST_' + self.frequency + '.csv')

        self.mae = mean_absolute_error(y_test, yh_test)
        self.mse = mean_squared_error(y_test, yh_test)
        self.r2 = r2_score(y_test, yh_test)

        print("R2: ", self.r2)
        print("MAE: ", self.mae)
        print("Median AE: ", median_absolute_error(y_test, yh_test))

    def _fit_learner(self,
                     X, y):
        pred_model = self.learning_algorithm(**self.learning_params)
        pred_model.fit(X, y)
        self.learner = pred_model
        return pred_model

    def _fit_learner_ensemble(self,
                              X, y):
        ensemble = dict()
        mfs = pd.DataFrame()

        # Calculate each sub-series meta-features and create models for ensemble
        for id in X[self.sub_series_column].unique():
            id_x_train = X[X[self.sub_series_column] == id]
            id_y_train = y[id_x_train.index]

            try:
                metafeatures = Metafeatures()
                id_mfs = metafeatures.compute(id_x_train, id_y_train)
                id_mfs[self.sub_series_column] = id
                if mfs.empty:
                    mfs = pd.DataFrame.from_dict(id_mfs)
                else:
                    mfs = pd.concat([mfs, pd.DataFrame.from_dict(id_mfs)], ignore_index=True)
            except Exception:
                pass

            model = self.learning_algorithm(**self.learning_params)
            model.fit(id_x_train, id_y_train)
            ensemble[id] = model
        mfs.set_index([self.sub_series_column])

        # Encode categorical values in metafeatures
        df_categorical = mfs.select_dtypes(include='object')
        for c_col in df_categorical.columns:
            le = LabelEncoder()
            le.fit(mfs[c_col])
            transformed = le.transform(mfs[c_col])
            mfs[c_col] = transformed

        # Missing values imputation
        fill_NaN = SimpleImputer(strategy='mean')
        mfs = mfs.replace([np.inf, -np.inf], np.nan)
        sub_series_column = mfs[self.sub_series_column].copy()
        mfs = pd.DataFrame(StandardScaler().fit_transform(mfs), columns=mfs.columns, index=mfs.index)
        mfs[self.sub_series_column] = sub_series_column
        mfs = mfs.dropna(axis=1, how='all')
        self.meta_features = pd.DataFrame(fill_NaN.fit_transform(mfs), columns=mfs.columns, index=mfs.index)
        self.learner = ensemble

    def _predict(self,
                 X):
        return self.learner.predict(X)

    def _predict_ensemble(self,
                          X):
        ensemble = self.learner
        mfs = self.meta_features
        k = self.k

        predictions = []
        for idx, r in X.iterrows():
            id = r[self.sub_series_column]
            row = r.values.reshape(1, -1)

            # Predict using the id's model in the ensemble
            p0 = None
            if id in ensemble:
                p0 = ensemble[id].predict(row)[0]

            # Calculate similarity
            pn = None
            if id in mfs:
                id_mfs = mfs.loc[id].values.reshape(1, -1)
                pn = []
                for key, model in ensemble.items():
                    if key in mfs.index:
                        p = model.predict(row)[0]
                        weight = cosine_similarity(mfs.loc[key].values.reshape(1, -1), id_mfs)
                        pn.append((p, weight))

                pn = sorted(pn, key=lambda t: t[1], reverse=True)[:k]

            # Calculate prediction based on weights
            if p0:
                if pn:
                    pn.append([(p0, 1)])
                    prediction = sum([p * w for p, w in pn]) / sum([w for _, w in pn])
                else:
                    pn = []
                    for model in ensemble.values():
                        pn.append(model.predict(row)[0])
                    prediction = np.mean(pn) * 0.2 + p0 * 0.8
            elif pn:
                prediction = sum([p * w for p, w in pn]) / sum([w for _, w in pn])
            else:
                pn = []
                for model in ensemble.values():
                    pn.append(model.predict(row)[0])
                prediction = np.mean(pn)
            predictions.append(prediction)

        return predictions

    def forecast(self,
                 ids=None):
        if not self.learner:
            raise Exception("Fit a model before making any predictions.")

        predict_func = self._predict_ensemble if self.prediction_strategy else self._predict

        if self.sub_series_column:
            if not ids:
                ids = self.x_forecast[self.sub_series_column].unique()
            else:
                ids = self.label_encoder.transform(ids)
            forecasts = dict()
            for s_id in ids:
                data = self.x_forecast[self.x_forecast[self.sub_series_column] == s_id]
                p = predict_func(data)
                forecasts[s_id] = p
                if self.standardizing_dict:
                    d = data.drop(self.sub_series_column,
                                  axis=1)
                    d[self.target] = p
                    sc = self.standardizing_dict[s_id]
                    transformed = pd.DataFrame(sc.inverse_transform(d),
                                               columns=d.columns,
                                               index=d.index)
                    p = transformed[self.target].values
                if self.label_encoder:
                    l = self.label_encoder.inverse_transform([int(s_id)])[0]
                else:
                    l = s_id
                print("Forecast id ", l, ": ", p)
                print("Time window: ", self.forecast_time_interval[s_id], "\n")
            return forecasts
        else:
            forecast = predict_func(self.x_forecast)
            print("Forecast : ", forecast)
            print("Time window: ", self.forecast_time_interval)
            return forecast
