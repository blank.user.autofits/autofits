import datetime
import time
import warnings

import numpy as np
import pandas as pd
from pandas.api.types import is_datetime64_any_dtype as is_datetime, is_timedelta64_dtype
from scipy.stats import iqr, entropy
from sklearn.linear_model import LinearRegression, Lasso
from sklearn.metrics import mean_absolute_error

from data_processing.data_transformations import split_dates
from utils.config import dprint


class FeatureConstructor:

    @staticmethod
    def get_stats_difference_between_observations(df_list,
                                                  column,
                                                  target,
                                                  frequency=None,
                                                  feature_name=None):
        start = time.time()
        if not feature_name:
            feature_name = "diff_" + column
        stats = pd.DataFrame(columns=['avg_' + feature_name,
                                      'std_dev_' + feature_name,
                                      'variance_' + feature_name,
                                      'sum_' + feature_name,
                                      'median_' + feature_name,
                                      'iqr_' + feature_name,
                                      'max_' + feature_name,
                                      'min_' + feature_name,
                                      'relative_dispersion_' + feature_name, ])

        for df_orig in df_list:
            df = df_orig[df_orig[target] != 0]
            times = []
            date_features = df.select_dtypes(include=[np.datetime64]).columns
            if column in date_features and not frequency:
                col = (df[column].copy() - datetime.datetime(1970, 1, 1)).dt.total_seconds()
            else:
                col = df[column]
            for idx in range(0, len(df) - 1):
                k1 = col.iloc[idx + 1]
                k2 = col.iloc[idx]
                diff = k1 - k2
                if frequency:
                    diff = diff / frequency
                times.append(diff)

            if not times:
                stats.loc[len(stats)] = [np.NaN] * len(stats.columns)
                continue

            max_v = np.NaN if not times else np.max(times)
            min_v = np.NaN if not times else np.min(times)
            mean = np.mean(times)
            try:
                std = np.std(times)
                var = np.variance(times)
            except Exception:
                std = np.NaN
                var = np.NaN

            try:
                rel_disp = std / abs(mean)
            except Exception:
                rel_disp = np.NaN

            stats.loc[len(stats)] = [mean,
                                     std,
                                     var,
                                     np.sum(times),
                                     np.median(times),
                                     iqr(times),
                                     max_v,
                                     min_v,
                                     rel_disp]
        stats = stats.dropna(axis=1, how='all')

        end = time.time()
        dprint("=> Feature stats_difference_between_observations (s): ", end - start)
        return stats

    @staticmethod
    def get_min_max_difference_timestamps(df_list,
                                          timestamp_column,
                                          target,
                                          frequency=None,
                                          feature_name='min_max_timestamp_difference'):
        start = time.time()

        diff = []
        for df in df_list:
            c = df[df[target] != 0][timestamp_column]
            if len(c) == 0:
                diff.append(np.NaN)
                continue
            min_ts = c.min()
            max_ts = c.max()
            if frequency:
                diff.append((max_ts - min_ts) / frequency)
            else:
                diff.append(max_ts - min_ts)

        end = time.time()
        dprint("=> Feature min_max_difference_timestamps (s): ", end - start)
        return pd.DataFrame.from_dict({feature_name: diff})

    @staticmethod
    def get_weekend_days_count(df_list,
                               timestamp_column,
                               feature_name='weekend_timestamps_count'):
        start = time.time()

        weekend_count = []
        for df in df_list:
            c = 0
            try:
                week_series = df[timestamp_column].dt.dayofweek
                for day in week_series:
                    if day >= 5:
                        c += 1
                weekend_count.append(c)
            except Exception:
                weekend_count.append(np.NaN)

        end = time.time()
        dprint("=> Feature weekend_days_count (s): ", end - start)
        return pd.DataFrame.from_dict({feature_name: weekend_count})

    @staticmethod
    def get_moving_average(df_list,
                           column,
                           subset_length,
                           feature_name='mov_avg'):
        start = time.time()

        avgs = []
        for df in df_list:
            lst = []
            for i in range(len(df) - subset_length):
                lst.append(df.iloc[i:i + subset_length][column].mean())
            avgs.append(lst)

        col_names = [feature_name + '_' + str(i) for i in range(len(df_list[0]) - subset_length)]
        mov_avg_df = pd.DataFrame(avgs, columns=col_names)

        end = time.time()
        dprint("=> Feature moving_average (s): ", end - start)
        return mov_avg_df

    @staticmethod
    def get_regressive_model_features(df_list,
                                      target,
                                      feature_name=''):
        start = time.time()

        models = [Lasso,
                  LinearRegression]
        cols = []
        for m in models:
            m_name = m().__class__.__name__
            cols.append(feature_name + m_name + '_forecast')
            cols.append(feature_name + m_name + '_mse')

        data = []
        for df in df_list:
            # Pre-process
            # for column in df:
            #     if is_datetime(df[column]):
            #         epoch_col = (df[column] - datetime.datetime(1970, 1, 1)).dt.total_seconds()
            #         df['epoch_' + column] = epoch_col
            #     elif is_timedelta64_dtype(df[column]):
            #         sec_col = df[column] / np.timedelta64(1, 's')
            #         df['sec_col_' + column] = sec_col

            df = split_dates(df.copy())

            test = df.tail(1)
            df = df.head(len(df) - 1)
            y = df[target]
            X = df.drop(target, axis=1)

            forecasts = []
            for m in models:
                with warnings.catch_warnings(record=True) as w:
                    try:
                        reg = m()
                        reg.fit(X, y)
                        y_test = test[target]
                        X_test = test.drop(target, axis=1)
                        pred = reg.predict(X_test)
                        forecasts.append(mean_absolute_error(y_test, pred))
                        forecasts.append(pred[0])
                    except Exception as e:
                        dprint("Prediction failed in regressive_model_features: ", str(e))
                        forecasts.append(np.NaN)
                        forecasts.append(np.NaN)
            data.append(forecasts)
        mov_avg_df = pd.DataFrame(data, columns=cols)

        end = time.time()
        dprint("=> Feature regressive_model_features (s): ", end - start)
        return mov_avg_df

    @staticmethod
    def get_2d_space_area(df_list,
                          timestamp_column,
                          target,
                          frequency=None,
                          feature_name='val_2d_space_area'):
        start = time.time()

        if not frequency:
            frequency = 1

        area = []
        for df in df_list:
            if frequency or not is_datetime(df[timestamp_column]):
                col = df[timestamp_column]
            else:
                col = (df[timestamp_column] - datetime.datetime(1970, 1, 1)).dt.total_seconds()
            try:
                min_ts = col.min()
                max_ts = col.max()
                min_targ = df[target].min()
                max_targ = df[target].max()
                if frequency:
                    area.append(((max_ts - min_ts) / frequency) * (max_targ - min_targ))
                else:
                    area.append((max_ts - min_ts) * (max_targ - min_targ))
            except Exception:
                area.append(np.NaN)

        end = time.time()
        dprint("=> Feature 2d_space_area (s): ", end - start)
        return pd.DataFrame.from_dict({feature_name: area})

    @staticmethod
    def get_missing_timestamps_count(df_list,
                                     groups,
                                     timestamp_column,
                                     feature_name='missing_timestamp_count'):
        start = time.time()

        missing_ts = []
        g_idx = 0
        for df in df_list:
            g = groups[g_idx].copy()
            missing = 0
            for idx in range(0, len(df) - 1):
                t1 = df[timestamp_column].iloc[idx + 1]
                t0 = df[timestamp_column].iloc[idx]
                g1 = g[g[timestamp_column] >= t0]
                g1 = g1[g1[timestamp_column] < t1]
                if g1.empty:
                    missing += 1
            t0 = df[timestamp_column].iloc[len(df) - 1]
            if g[g[timestamp_column] >= t0].empty:
                missing += 1
            missing_ts.append(missing)
            g_idx += 1

        end = time.time()
        dprint("=> Feature missing_timestamps_count (s): ", end - start)
        return pd.DataFrame.from_dict({feature_name: missing_ts})

    @staticmethod
    def get_timestamps_target_avg_diff_mul(df_list,
                                           timestamp_column,
                                           target,
                                           frequency=None,
                                           feature_name='timestamps_target_avg_diff_mul'):
        """
        For each subsequence, get the average of the multiplication between timestamps and the target variable.
        Dates are converted to epoch time.

        :param frequency:
        :param feature_name:
        :param df_list:
        :param timestamp_column:
        :param target:
        :return:
        """
        start = time.time()

        avgs = []
        for df in df_list:
            diffs = []
            for idx in range(0, len(df) - 1):
                t1 = df[timestamp_column].iloc[idx + 1]
                t2 = df[timestamp_column].iloc[idx]
                y1 = df[target].iloc[idx + 1]
                y2 = df[target].iloc[idx]
                diff = t1 - t2
                if frequency:
                    diff = diff / frequency
                diffs.append(diff * (y1 - y2))

            avgs.append(np.mean(diffs))

        end = time.time()
        dprint("=> Feature timestamps_target_avg_diff_mul (s): ", end - start)
        return pd.DataFrame.from_dict({feature_name: avgs})

    @staticmethod
    def get_timestamps_target_mul_avg(df_list,
                                      timestamp_column,
                                      target,
                                      feature_name='timestamps_target_mul_avg'):
        """
        For each subsequence, get the average of the multiplication between timestamps and the target variable.
        Dates are converted to epoch time.

        :param feature_name:
        :param df_list:
        :param timestamp_column:
        :param target:
        :return:
        """
        start = time.time()

        avgs = []
        for df in df_list:
            if df.empty:
                avgs.append(0)
                continue
            try:
                epoch_col = (df[timestamp_column] - datetime.datetime(1970, 1, 1)).dt.total_seconds()
            except TypeError:
                epoch_col = df[timestamp_column] / np.timedelta64(1, 's')
                # epoch_col = df[timestamp_column].apply(lambda x: x.nanoseconds)
                # epoch_col = df[timestamp_column].values
                # epoch_col = df[timestamp_column].apply(lambda x: x.item().total_seconds())

            avg = (epoch_col * df[target]).mean()
            avgs.append(avg)

        end = time.time()
        dprint("=> Feature timestamps_target_mul_avg (s): ", end - start)
        return pd.DataFrame.from_dict({feature_name: avgs})

    @staticmethod
    def get_relative_dispersion(df_list,
                                column,
                                feature_name=None):
        """
        For each subsequence, get the relative dipsersion measure.
        Dates are converted to epoch time.

        :param frequency:
        :param feature_name:
        :param df_list:
        :param column:
        :return:
        """
        start = time.time()
        if not feature_name:
            feature_name = "relative_dispersion_" + column

        rel_disp_lst = []
        for df in df_list:
            try:
                if is_datetime(df[column]):
                    epoch_col = (df[column] - datetime.datetime(1970, 1, 1)).dt.total_seconds()
                    std = np.std(epoch_col)
                    mean = abs(np.mean(epoch_col))
                elif is_timedelta64_dtype(df[column]):
                    sec_col = df[column] / np.timedelta64(1, 's')
                    std = np.std(sec_col)
                    mean = abs(np.mean(sec_col))
                else:
                    std = np.std(df[column])
                    mean = np.mean(df[column])
                if mean != 0:
                    rel_disp = std / abs(mean)
                else:
                    rel_disp = np.NaN
            except Exception as e:
                rel_disp = np.NaN
            rel_disp_lst.append(rel_disp)

        end = time.time()
        dprint("=> Feature relative_dispersion_", column, " (s): ", end - start)

        return pd.DataFrame.from_dict({feature_name: rel_disp_lst})

    @staticmethod
    def get_weekday_peak(df_list,
                         timestamp_column,
                         target,
                         feature_name='weekday_peak'):
        start = time.time()

        peaks = []
        for df in df_list:
            c = 0
            try:
                max_target = df[target].idxmax()
                ts = df.loc[max_target][timestamp_column]
                peaks.append(ts.dayofweek)
            except Exception:
                peaks.append(np.NaN)

        end = time.time()
        dprint("=> Feature weekday_peak (s): ", end - start)
        return pd.DataFrame.from_dict({feature_name: peaks})

    @staticmethod
    def get_entropy(df_list,
                    column,
                    feature_name=None):
        start = time.time()
        if not feature_name:
            feature_name = "entropy_" + column

        ent = []
        for df in df_list:
            value, counts = np.unique(df[column], return_counts=True)
            ent.append(entropy(counts))

        end = time.time()
        dprint("=> Feature entropy_" + column + " (s): ", end - start)
        return pd.DataFrame.from_dict({feature_name: ent})
