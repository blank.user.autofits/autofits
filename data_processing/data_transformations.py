import numpy as np
import pandas as pd

TEMP_NAME = 'temporary_target_column_5687328732221'


def split_data_into_subsets(X,
                            y,
                            target,
                            subset_length,
                            sub_series_column=None,
                            timestamp_column=None,
                            frequency=None,
                            aggregating_strategy='sum',
                            imputation_strategy='zero'):
    if not sub_series_column:
        df_subsets = []
        groups = []
        X[target] = y
        d_x, grps = resample_data(X,
                                  frequency,
                                  strategy=aggregating_strategy,
                                  key=timestamp_column,
                                  return_groups=True,
                                  imputation_strategy=imputation_strategy)
        d_x = d_x.reset_index()
        x_forecast_resampled = d_x.iloc[len(d_x) - subset_length:len(d_x)]
        x_forecast_group = grps[len(d_x) - subset_length:len(d_x)]

        for i in range(len(d_x) - subset_length):
            df_subsets.append(d_x.iloc[i:i + subset_length])
            lst_grps = grps[i:i + subset_length]
            groups.append(pd.concat(lst_grps,
                                    ignore_index=True))

        new_y = d_x[target].iloc[subset_length:]
    else:
        df_subsets = []
        groups = []

        X[target] = y
        gb = X.groupby(sub_series_column)
        df_list = [gb.get_group(x) for x in gb.groups]
        new_y = []
        x_forecast_resampled = dict()
        x_forecast_group = dict()
        for d in df_list:
            df_id = d[sub_series_column].iloc[0]
            new_d, grps = resample_data(d,
                                        frequency,
                                        strategy=aggregating_strategy,
                                        key=timestamp_column,
                                        return_groups=True,
                                        imputation_strategy=imputation_strategy)
            new_d[sub_series_column] = df_id
            new_d = new_d.reset_index()

            if len(new_d) >= subset_length:
                x_forecast_resampled[df_id] = new_d.iloc[len(new_d) - subset_length:len(new_d)]
                lst_grps = grps[len(new_d) - subset_length:len(new_d)]
                x_forecast_group[df_id] = pd.concat(lst_grps,
                                                    ignore_index=True)

            for i in range(len(new_d) - subset_length):
                sub = new_d.iloc[i:i + subset_length]
                lst_grps = grps[i:i + subset_length]
                df_subsets.append(sub)
                groups.append(pd.concat(lst_grps,
                                        ignore_index=True))
                new_y.append(new_d[target].iloc[i + subset_length])

    return df_subsets, np.array(new_y), groups, x_forecast_resampled, x_forecast_group


def resample_data(df,
                  frequency,
                  strategy='sum',
                  key=None,
                  return_groups=False,
                  imputation_strategy='zero'):
    # Resample data according to strategy
    groups = []
    if strategy == 'mean':
        gb = df.groupby(pd.Grouper(key=key,
                                   freq=frequency))
        for g, grp in gb:
            groups.append(grp)
        df = gb.mean()
    elif strategy == 'mode':
        gb = df.groupby(pd.Grouper(key=key,
                                   freq=frequency))
        for g, grp in gb:
            groups.append(grp)
        df = gb.mode()
    else:  # Default to sum
        gb = df.groupby(pd.Grouper(key=key,
                                   freq=frequency))
        for g, grp in gb:
            groups.append(grp)
        df = gb.sum()

    if imputation_strategy == 'ffill':
        df = df.fillna(method='ffill')
    elif imputation_strategy == 'mean':
        df = df.fillna(df.mean())
    elif imputation_strategy == 'mode':
        df = df.fillna(df.mode())
    else:
        df = df.fillna(0)

    if not return_groups:
        return df
    return df, groups


def add_missing_timestamps(X,
                           y,
                           timestamp_column,
                           frequency,
                           strategy):
    """
    Add any missing timestamps. Converts an irregular forecasting dataset into a regular one

    :param X: Dataframe
    :param y:
    :param timestamp_column: Str. Column containing the timestamps
    :param frequency: How the timestamps are
    recorded. Eg: 'H' implies hourly timestamps; '2D' implies every 2 days.
    More info: https://pandas.pydata.org/pandas-docs/stable/user_guide/timeseries.html#timeseries-offset-aliases
    :param strategy: How to null-fill the target column. Options: constant, mean, median
    """

    # Append the datasets (they will be separated later)
    X[TEMP_NAME] = y

    # Value to input on missing entries in the target column
    input_value = 0
    if strategy == 'mean':
        input_value = np.mean(y)
    elif strategy == 'median':
        input_value = np.median(y)

    # Get missing timestamps
    timetamps = list(X[timestamp_column])
    min_ts = min(timetamps)
    max_ts = max(timetamps)
    all_tim = list(pd.date_range(min_ts, max_ts, freq=frequency))
    missing_ts = set(all_tim).difference(timetamps)

    # Add skipped timestamps to the dataset
    for ts in missing_ts:
        row_data = dict(zip(list(X.columns), [np.nan] * len(X.columns)))
        row_data[TEMP_NAME] = input_value
        row_data[timestamp_column] = ts
        X = X.append(row_data,
                     ignore_index=True)

    # Sort by timestamp
    X.sort_values(by=[timestamp_column], inplace=True)

    # Split target and predictors and return
    y = X[TEMP_NAME]
    return X.drop(TEMP_NAME, axis=1), y


def split_dates(df):
    dates_df_t_delta = df.select_dtypes(include=[np.timedelta64])
    dates_df_date_t = df.select_dtypes(include=[np.datetime64])
    if dates_df_t_delta.empty and dates_df_date_t.empty:
        return df
    df_list = []
    for col in dates_df_t_delta.columns:
        components_df = df[col].dt.components
        new_col_names = [col + '_' + c for c in components_df.columns]
        components_df.columns = new_col_names
        df_list.append(components_df)

    for col in dates_df_date_t.columns:
        components = dates_df_date_t[col].apply(split_date_time_value)
        columns = [col + '_year',
                   col + '_month',
                   col + '_day',
                   col + '_hour',
                   col + '_minute',
                   col + '_second',
                   col + '_microsecond']
        df_date = pd.DataFrame([[dy, dm, dd, th, tm, ts, tms] for dy, dm, dd, th, tm, ts, tms in components.values],
                               columns=columns, index=df.index)
        df_list.append(df_date)

    new_features = pd.concat(df_list, axis=1)
    df = df.drop(dates_df_t_delta.columns, axis=1, errors='ignore')
    df = df.drop(dates_df_date_t.columns, axis=1, errors='ignore')
    return pd.concat([df, new_features], axis=1)


def split_date_time_value(dt):
    return dt.year, dt.month, dt.day, dt.hour, dt.minute, dt.second, dt.microsecond


