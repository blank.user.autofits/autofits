from random import randint
from unittest import TestCase

import numpy as np
import pandas as pd

from data_processing.data_transformations import resample_data, add_missing_timestamps


class TestDataTransformations(TestCase):

    def test_resample_data(self):
        # Arrange
        timestamps_orig = list(pd.date_range('2021-01-01', '2021-01-30', freq='D'))
        timestamps_intended = list(pd.date_range('2021-01-01', '2021-01-30', freq='2D'))
        timestamps_missing = timestamps_orig.copy()
        del timestamps_missing[3]  # Remove 2021-01-04
        del timestamps_missing[7]  # Remove 2021-01-09
        del timestamps_missing[11]  # Remove 2021-01-14

        X = pd.DataFrame({'timestamp': timestamps_missing,
                          'str_col': [3] * len(timestamps_missing)})
        X.set_index('timestamp', inplace=True)

        y = pd.Series([1] * len(timestamps_missing))
        y_intended = [2, 1, 2, 2, 1, 2, 1, 2, 2, 2, 2, 2, 2, 2, 2]

        # Act
        new_X, new_y = resample_data(X,
                                     y,
                                     '2D',
                                     'sum')

        # Assert
        self.assertListEqual(timestamps_intended, list(new_X.index.values))
        self.assertListEqual(y_intended, list(new_y))

    def test_add_missing_stamps_freq_1_day(self):
        # Arrange
        timestamps_orig = list(pd.date_range('2021-01-01', '2021-01-30', freq='D'))
        timestamps_missing = timestamps_orig.copy()
        del timestamps_missing[3]  # Remove 2021-01-04
        del timestamps_missing[7]  # Remove 2021-01-09
        del timestamps_missing[11]  # Remove 2021-01-14

        y_intended = [1] * len(timestamps_orig)
        y_intended[3] = y_intended[8] = y_intended[13] = 0

        X = pd.DataFrame({'timestamp': timestamps_missing,
                          'str_col': ['just a string'] * len(timestamps_missing)})
        y = pd.Series([1] * len(timestamps_missing))

        # Act
        new_X, new_y = add_missing_timestamps(X,
                                              y,
                                              'timestamp',
                                              'D',
                                              'constant')

        # Assert
        self.assertListEqual(timestamps_orig, list(new_X['timestamp'].values))
        self.assertListEqual(y_intended, list(new_y))

    def test_add_missing_stamps_freq_1_hour(self):
        # Arrange
        timestamps_orig = list(pd.date_range('2021-01-01', '2021-01-02', freq='H'))
        timestamps_missing = timestamps_orig.copy()
        del timestamps_missing[3]  # Remove 03:00
        del timestamps_missing[7]  # Remove 08:00
        del timestamps_missing[11]  # Remove 13:00

        y_intended = [1] * len(timestamps_orig)
        y_intended[3] = y_intended[8] = y_intended[13] = 0

        X = pd.DataFrame({'timestamp': timestamps_missing,
                          'str_col': ['just a string'] * len(timestamps_missing)})
        y = pd.Series([1] * len(timestamps_orig))

        # Act
        new_X, new_y = add_missing_timestamps(X,
                                              y,
                                              'timestamp',
                                              'H',
                                              'constant')

        # Assert
        self.assertListEqual(timestamps_orig, list(new_X['timestamp'].values))
        self.assertListEqual(y_intended, list(new_y))

    def test_add_missing_stamps_freq_7_day(self):
        # Arrange
        timestamps_orig = list(pd.date_range('2021-01-01', '2021-04-30', freq='7D'))
        timestamps_missing = timestamps_orig.copy()
        del timestamps_missing[3]  # Remove 2021-01-22
        del timestamps_missing[7]  # Remove 2021-02-26
        del timestamps_missing[11]  # Remove 2021-04-02

        y = [randint(0, 5) for _ in range(0, len(timestamps_orig))]
        del y[3]
        del y[7]
        del y[11]
        avg = np.mean(y)

        X = pd.DataFrame({'timestamp': timestamps_missing,
                          'str_col': [2] * len(timestamps_missing)})
        # Act
        new_X, new_y = add_missing_timestamps(X,
                                              y,
                                              'timestamp',
                                              '7D',
                                              'mean')

        # Assert
        self.assertListEqual(timestamps_orig, list(new_X['timestamp'].values))
        self.assertEqual(avg, list(new_y)[3])
        self.assertEqual(avg, list(new_y)[8])
        self.assertEqual(avg, list(new_y)[13])

    def test_add_missing_stamps_freq_3_hour(self):
        # Arrange
        timestamps_orig = list(pd.date_range('2021-01-01', '2021-01-03', freq='3H'))
        timestamps_missing = timestamps_orig.copy()
        del timestamps_missing[3]  # Remove 2021-01-01 09:00:00
        del timestamps_missing[7]  # Remove 2021-01-02 00:00:00
        del timestamps_missing[11]  # Remove 2021-01-02 15:00:00

        y = [randint(0, 5) for _ in range(0, len(timestamps_orig))]
        del y[3]
        del y[7]
        del y[11]
        avg = np.mean(y)

        X = pd.DataFrame({'timestamp': timestamps_missing,
                          'str_col': [2] * len(timestamps_missing)})

        # Act
        new_X, new_y = add_missing_timestamps(X,
                                              y,
                                              'timestamp',
                                              '3H',
                                              'mean')

        # Assert
        self.assertListEqual(timestamps_orig, list(new_X['timestamp'].values))
        self.assertEqual(avg, list(new_y)[3])
        self.assertEqual(avg, list(new_y)[8])
        self.assertEqual(avg, list(new_y)[13])

    def test_resample_data_reserve(self):
        # Arrange
        data = pd.read_csv('../assets/reserve.csv')
        y = data['reserve_visitors']
        x = data.drop('reserve_visitors', axis=1)
        id = x['air_store_id'].iloc[0]
        x['reserve_datetime'] = pd.to_datetime(x['reserve_datetime'])
        x.set_index('reserve_datetime', inplace=True)

        # Act
        x, y = resample_data(x, y, '6H')
        x['air_store_id'] = id

        # Assert
        print(x)
        print(y)
