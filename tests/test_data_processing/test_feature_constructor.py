import os
from unittest import TestCase

import pandas as pd

from data_processing.data_transformations import split_data_into_subsets
from data_processing.feature_constructor import FeatureConstructor


class TestFeatureConstructor(TestCase):

    def setUp(self) -> None:
        path = os.path.dirname(os.path.realpath(__file__)) + '/../'
        self.air_reserve = path + '../data/restaurant/air_reserve.csv'
        self.air_store_info = path + '../data/restaurant/air_store_info.csv'
        self.air_visit_data = path + '../data/restaurant/air_visit_data.csv'
        self.date_info = path + '../data/restaurant/date_info.csv'
        self.hpg_reserve = path + '../data/restaurant/hpg_reserve.csv'
        self.hpg_store_info = path + '../data/restaurant/hpg_store_info.csv'
        self.sample_submission = path + '../data/restaurant/sample_submission.csv'
        self.store_id_relation = path + '../data/restaurant/store_id_relation.csv'

    def test_get_average_time_between_timestamps(self):
        # Arrange
        df_reserve = pd.read_csv(self.air_reserve, encoding='latin-1')
        df = df_reserve[df_reserve['air_store_id'] == 'air_6b15edd1b4fbb96a']

        schema = {'air_store_id': 'categorical',
                  'visit_datetime': 'date',
                  'reserve_datetime': 'timestamp',
                  'reserve_visitors': 'numeric'}
        ts_col = df['reserve_datetime'].copy()
        df['reserve_datetime'] = pd.to_datetime(ts_col)

        df.sort_values(by=['reserve_datetime'], inplace=True)

        y = df['reserve_visitors']
        X = df.drop('reserve_visitors', axis=1)

        gb = split_data_into_subsets(X, y, 'reserve_visitors', 6)

        f1 = FeatureConstructor.get_average_difference_between_observations(gb, 'reserve_datetime')
        f2 = FeatureConstructor.get_min_max_difference_timestamps(gb, 'reserve_datetime')
        f3 = FeatureConstructor.get_weekend_days_count(gb, 'reserve_datetime')
        print(len(f1))
        print(len(f2))
        print(len(f3))
        print(len(gb))
