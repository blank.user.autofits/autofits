import datetime
import os
from unittest import TestCase

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestRegressor
from sklearn.linear_model import Lasso

from workflows.auto_fits import AutoFITS
from workflows.auto_fits_vest import AutoFV
from workflows.vest import VESTForecaster


########################################################################
# - This file contains additional experiments made during development  #
# - These "tests" should not be seen as how AutoFITS should be used    #
########################################################################

class TestIrregularForecaster(TestCase):

    def setUp(self) -> None:
        path = os.path.dirname(os.path.realpath(__file__)) + '/../'
        self.vostok = path + '../data/vostok/vostok_ice_core_data.csv'
        self.dandak = path + '../data/dandak.csv'
        self.wanxiang = path + '../data/wanxiang.csv'
        self.m5 = path + '../data/m5/'
        self.air_reserve = path + '../data/restaurant/air_reserve.csv'
        self.air_store_info = path + '../data/restaurant/air_store_info.csv'
        self.air_visit_data = path + '../data/restaurant/air_visit_data.csv'
        self.date_info = path + '../data/restaurant/date_info.csv'
        self.hpg_reserve = path + '../data/restaurant/hpg_reserve.csv'
        self.hpg_store_info = path + '../data/restaurant/hpg_store_info.csv'
        self.sample_submission = path + '../data/restaurant/sample_submission.csv'
        self.store_id_relation = path + '../data/restaurant/store_id_relation.csv'

    def test_global(self):
        # Arrange
        df = pd.read_csv(self.air_reserve, encoding='latin-1')

        schema = {'air_store_id': 'sub_series_id',
                  'visit_datetime': 'timestamp',
                  'reserve_datetime': 'date',
                  'reserve_visitors': 'numeric'}

        ts_col = df['visit_datetime'].copy()
        df['visit_datetime'] = pd.to_datetime(ts_col)

        # Filter dates
        print("Before: ", len(df))
        df = df[df['visit_datetime'] > '2016-10-26']
        print("After: ", len(df))

        df.sort_values(by=['visit_datetime'], inplace=True)
        df['visit_datetime'] = pd.to_datetime(ts_col)
        target = 'reserve_visitors'

        # learners = [Ridge, Lasso, RandomForestRegressor]
        learners = [Lasso, RandomForestRegressor]
        results_fits = []
        results_baseline = []
        results_vest = []
        results_fits_vest = []

        frequency_lags = {
            '1D': 10,
            '2D': 10,
            '3D': 10,
            '4D': 10,
            '5D': 10,
            '6D': 10,
            '7D': 10,
            '8D': 10,
            '9D': 10,
            '10D': 10,
            '11D': 10,
            '12D': 10,
            '13D': 10,
            '14D': 10,
            '15D': 7,
            '16D': 7,
            '17D': 7,
            '18D': 7,
            '19D': 7,
            '20D': 7,
            '21D': 3,
            '22D': 3,
            '23D': 3,
            '24D': 3,
            '25D': 3,
            '26D': 3,
            '27D': 3,
            '28D': 2,
            '29D': 2,
            '30D': 2,
            '31D': 2}

        prediction_strategy = None
        # prediction_strategy = 'ensemble'

        for freq, lag in frequency_lags.items():
            for l in learners:
                print("#############################")
                print(l, " , ", freq)
                print("#############################")

                print("\n=> VEST")
                try:
                    rf_vest = VESTForecaster(schema=schema,
                                             target=target,
                                             frequency=freq,
                                             learning_algorithm=l,
                                             lag_size=lag,
                                             prediction_strategy=prediction_strategy,
                                             imputation_strategy='zero')
                    rf_vest.fit(df.copy())
                    results_vest.append([l().__class__.__name__,
                                         freq,
                                         rf_vest.zero_count,
                                         rf_vest.total_cells,
                                         rf_vest.mse,
                                         rf_vest.mae,
                                         rf_vest.r2,
                                         rf_vest.data_len,
                                         ""])
                    rf_vest.forecast()
                except Exception as e:
                    results_vest.append([l().__class__.__name__,
                                         freq,
                                         np.NaN,
                                         np.NaN,
                                         np.NaN,
                                         np.NaN,
                                         np.NaN,
                                         np.NaN,
                                         str(e)])

                y = df[target]
                X = df.drop(target, axis=1)
                try:
                    irf_fv = AutoFV(schema,
                                    frequency=freq,
                                    target=target,
                                    learning_algorithm=l,
                                    lag_size=lag,
                                    percentile=80,
                                    prediction_strategy=prediction_strategy,
                                    imputation_strategy='zero',
                                    test_size=0.1)

                    print("\n=> AutoFV")
                    irf_fv.fit(X, y)
                    irf_fv.forecast()
                    results_fits_vest.append([l().__class__.__name__,
                                              freq,
                                              irf_fv.zero_count,
                                              irf_fv.total_cells,
                                              irf_fv.mse,
                                              irf_fv.mae,
                                              irf_fv.r2,
                                              irf_fv.data_len,
                                              ""])
                except Exception as e:
                    results_fits_vest.append([l().__class__.__name__,
                                              freq,
                                              np.NaN,
                                              np.NaN,
                                              np.NaN,
                                              np.NaN,
                                              np.NaN,
                                              np.NaN,
                                              str(e)])
                try:
                    irf = AutoFITS(schema,
                                   frequency=freq,
                                   target=target,
                                   learning_algorithm=l,
                                   lag_size=lag,
                                   percentile=80,
                                   prediction_strategy=prediction_strategy,
                                   imputation_strategy='zero',
                                   test_size=0.1,
                                   cache_data=True)

                    print("\n=> AutoFITS")
                    irf.fit(X, y)
                    irf.forecast()
                    results_fits.append([l().__class__.__name__,
                                         freq,
                                         irf.zero_count,
                                         irf.total_cells,
                                         irf.mse,
                                         irf.mae,
                                         irf.r2,
                                         irf.data_len,
                                         ""])
                except Exception as e:
                    results_fits.append([l().__class__.__name__,
                                         freq,
                                         np.NaN,
                                         np.NaN,
                                         np.NaN,
                                         np.NaN,
                                         np.NaN,
                                         np.NaN,
                                         str(e)])
                irf.method = "baseline"

                try:
                    print("=> Baseline")
                    irf.fit(X, y)
                    irf.forecast()
                    print('')
                    results_baseline.append([l().__class__.__name__,
                                             freq,
                                             irf.zero_count,
                                             irf.total_cells,
                                             irf.mse,
                                             irf.mae,
                                             irf.r2,
                                             irf.data_len,
                                             ""])
                except Exception as e:
                    results_baseline.append([l().__class__.__name__,
                                             freq,
                                             np.NaN,
                                             np.NaN,
                                             np.NaN,
                                             np.NaN,
                                             np.NaN,
                                             np.NaN,
                                             str(e)])

                df_fits = pd.DataFrame(results_fits,
                                       columns=['learner', 'frequency', 'zero_count', 'total_cells', 'mse', 'mae',
                                                'r2',
                                                'data_len', 'exception'])
                df_fits_vest = pd.DataFrame(results_fits_vest,
                                            columns=['learner', 'frequency', 'zero_count', 'total_cells', 'mse', 'mae',
                                                     'r2',
                                                     'data_len', 'exception'])
                df_baseline = pd.DataFrame(results_baseline,
                                           columns=['learner', 'frequency', 'zero_count', 'total_cells', 'mse',
                                                    'mae',
                                                    'r2',
                                                    'data_len', 'exception'])
                df_vest = pd.DataFrame(results_vest,
                                       columns=['learner', 'frequency', 'zero_count', 'total_cells', 'mse', 'mae', 'r2',
                                                'data_len', 'exception'])
            if not prediction_strategy:
                df_fits.to_csv('fits_results_1.csv', index=False)
                df_fits_vest.to_csv('fv_results_1.csv', index=False)
                df_baseline.to_csv('baseline_results_1.csv', index=False)
                df_vest.to_csv('vest_results_1.csv', index=False)
            else:
                df_fits.to_csv('fits_results_1_ensemble_4.csv', index=False)
                df_fits_vest.to_csv('fv_results_1_ensemble_4.csv', index=False)
                df_baseline.to_csv('baseline_results_1_ensemble_4.csv', index=False)
                df_vest.to_csv('vest_results_1_ensemble_4.csv', index=False)

    def test_restaurants_lag(self):
        # Arrange
        df = pd.read_csv(self.air_reserve, encoding='latin-1')

        schema = {'air_store_id': 'sub_series_id',
                  'visit_datetime': 'timestamp',
                  'reserve_datetime': 'date',
                  'reserve_visitors': 'numeric'}

        ts_col = df['visit_datetime'].copy()
        df['visit_datetime'] = pd.to_datetime(ts_col)

        # Filter dates
        print("Before: ", len(df))
        df = df[df['visit_datetime'] > '2016-10-26']
        print("After: ", len(df))

        df.sort_values(by=['visit_datetime'], inplace=True)
        df['visit_datetime'] = pd.to_datetime(ts_col)
        target = 'reserve_visitors'

        results_baseline = []
        frequencies = ['1D', '7D', '14D', '21D', '28D']
        lags = list(range(1, 11))

        for freq in frequencies:
            for lag in lags:
                print("#############################")
                print(lag, " , ", freq)
                print("#############################")

                y = df[target]
                X = df.drop(target, axis=1)
                try:
                    irf = AutoFITS(schema,
                                   frequency=freq,
                                   target=target,
                                   method='baseline',
                                   learning_algorithm=RandomForestRegressor,
                                   lag_size=lag,
                                   percentile=80,
                                   imputation_strategy='zero',
                                   test_size=0.1)

                    print("=> Baseline")
                    irf.fit(X, y)
                    # irf.forecast()
                    print('')
                    results_baseline.append([lag,
                                             freq,
                                             irf.zero_count,
                                             irf.total_cells,
                                             irf.mse,
                                             irf.mae,
                                             irf.r2,
                                             irf.data_len,
                                             len(irf.unique_entities),
                                             ""])
                except Exception as e:
                    results_baseline.append([lag,
                                             freq,
                                             np.NaN,
                                             np.NaN,
                                             np.NaN,
                                             np.NaN,
                                             np.NaN,
                                             np.NaN,
                                             np.NaN,
                                             str(e)])

                df_baseline = pd.DataFrame(results_baseline,
                                           columns=['lag_size', 'frequency', 'zero_count', 'total_cells', 'mse',
                                                    'mae',
                                                    'r2',
                                                    'data_len', 'unique_entities',
                                                    'exception'])

            df_baseline.to_csv('restaurants_lag_analysis.csv', index=False)


    def test_vostok(self):
        # Arrange
        df = pd.read_csv(self.vostok)

        schema = {'age (yrs bp)': 'timestamp',
                  'co2': 'numeric'}

        df['age (yrs bp)'] = df['age (yrs bp)'].apply(lambda d: pd.Timestamp(1950, 1, 1) + pd.DateOffset(hours=d))
        learners = [Lasso, RandomForestRegressor]

        results_fits = []
        results_baseline = []
        results_vest = []
        results_fits_vest = []

        lag_size = 7

        for f in range(2750, 2751, 250):
            freq = str(f) + 'H'
            for l in learners:
                print("#############################")
                print(l, " , ", freq)
                print("#############################")

                rf_vest = VESTForecaster(schema=schema,
                                         target='co2',
                                         frequency=freq,
                                         learning_algorithm=l,
                                         lag_size=lag_size,
                                         resampling_strategy='mean',
                                         imputation_strategy='ffill')

                print("=> VEST")
                rf_vest.fit(df.copy())
                rf_vest.forecast()

                y = df['co2']
                X = df.drop('co2', axis=1)
                irf = AutoFITS(schema,
                               frequency=freq,
                               target='co2',
                               learning_algorithm=l,
                               resampling_strategy='mean',
                               percentile=80,
                               lag_size=lag_size,
                               imputation_strategy='ffill')

                irf_baseline = AutoFITS(schema,
                                        method='baseline',
                                        frequency=freq,
                                        learning_algorithm=l,
                                        resampling_strategy='mean',
                                        target='co2',
                                        lag_size=lag_size,
                                        imputation_strategy='ffill')
                fv_irf = AutoFV(schema,
                                frequency=freq,
                                target='co2',
                                learning_algorithm=l,
                                aggregating_strategy='mean',
                                percentile=80,
                                lag_size=lag_size,
                                imputation_strategy='ffill')

                print("\n=> AutoFITS")
                irf.fit(X, y)
                irf.forecast()
                print("=> FITS_VEST")
                fv_irf.fit(X, y)
                fv_irf.forecast(X, y)
                print("=> Baseline")
                irf_baseline.fit(X, y)
                irf_baseline.forecast(X, y)
                print('')

                results_fits.append([l().__class__.__name__,
                                     freq,
                                     irf.zero_count,
                                     irf.total_cells,
                                     irf.mse,
                                     irf.mae,
                                     irf.r2,
                                     irf.data_len])
                results_fits_vest.append([l().__class__.__name__,
                                          freq,
                                          fv_irf.zero_count,
                                          fv_irf.total_cells,
                                          fv_irf.mse,
                                          fv_irf.mae,
                                          fv_irf.r2,
                                          fv_irf.data_len])
                results_baseline.append([l().__class__.__name__,
                                         freq,
                                         irf_baseline.zero_count,
                                         irf_baseline.total_cells,
                                         irf_baseline.mse,
                                         irf_baseline.mae,
                                         irf_baseline.r2,
                                         irf_baseline.data_len])
                results_vest.append([l().__class__.__name__,
                                     freq,
                                     rf_vest.zero_count,
                                     rf_vest.total_cells,
                                     rf_vest.mse,
                                     rf_vest.mae,
                                     rf_vest.r2,
                                     rf_vest.data_len])

        df_fits = pd.DataFrame(results_fits,
                               columns=['learner', 'frequency', 'zero_count', 'total_cells', 'mse', 'mae', 'r2',
                                        'data_len'])
        df_fits_vest = pd.DataFrame(results_fits_vest,
                                    columns=['learner', 'frequency', 'zero_count', 'total_cells', 'mse', 'mae', 'r2',
                                             'data_len'])
        df_baseline = pd.DataFrame(results_baseline,
                                   columns=['learner', 'frequency', 'zero_count', 'total_cells', 'mse', 'mae', 'r2',
                                            'data_len'])
        df_vest = pd.DataFrame(results_vest,
                               columns=['learner', 'frequency', 'zero_count', 'total_cells', 'mse', 'mae', 'r2',
                                        'data_len'])
        df_fits.to_csv('fits_results_' + 'vostok' + '.csv', index=False)
        df_fits_vest.to_csv('fv_results_' + 'vostok' + '.csv', index=False)
        df_baseline.to_csv('baseline_results_' + 'vostok' + '.csv', index=False)
        df_vest.to_csv('vest_results_' + 'vostok' + '.csv', index=False)

    def convert_hour(self, d):
        return datetime.timedelta(hours=d)

    def test_dandak(self):
        # Arrange
        df = pd.read_csv(self.dandak)

        schema = {'AgeAD': 'timestamp',
                  'd18O': 'numeric',
                  'AgeErr': 'numeric'}

        target = 'd18O'
        timestamp = 'AgeAD'
        # df[timestamp] = df[timestamp].apply(lambda d: pd.Timestamp(1950, 1, 1) + pd.DateOffset(hours=d))

        df[timestamp] = df[timestamp].apply(lambda d: self.convert_hour(d))
        learners = [Lasso, RandomForestRegressor]

        results_fits = []
        results_baseline = []
        results_vest = []
        results_fits_vest = []

        for f in np.arange(0.25,
                           5.1, 0.25):
            freq = str(f) + 'H'
            for l in learners:
                print("#############################")
                print(l, " , ", freq)
                print("#############################")

                rf_vest = VESTForecaster(schema=schema,
                                         target=target,
                                         frequency=freq,
                                         learning_algorithm=l,
                                         lag_size=6,
                                         resampling_strategy='mean',
                                         imputation_strategy='ffill')

                print("=> VEST")
                rf_vest.fit(df.copy())
                # rf_vest.forecast()

                y = df[target]
                X = df.drop(target, axis=1)
                irf = AutoFITS(schema,
                               frequency=freq,
                               target=target,
                               learning_algorithm=l,
                               resampling_strategy='mean',
                               percentile=80,
                               lag_size=6,
                               imputation_strategy='ffill')

                irf_baseline = AutoFITS(schema,
                                        method='baseline',
                                        frequency=freq,
                                        learning_algorithm=l,
                                        resampling_strategy='mean',
                                        target=target,
                                        lag_size=6,
                                        imputation_strategy='ffill')
                fv_irf = AutoFV(schema,
                                frequency=freq,
                                target=target,
                                learning_algorithm=l,
                                aggregating_strategy='mean',
                                percentile=80,
                                lag_size=6,
                                imputation_strategy='ffill')

                print("\n=> AutoFITS")
                irf.fit(X, y)
                # irf.forecast()
                print("=> FITS_VEST")
                fv_irf.fit(X, y)
                # fv_irf.forecast(X, y)
                print("=> Baseline")
                irf_baseline.fit(X, y)
                # irf_baseline.forecast(X, y)
                print('')

                results_fits.append([l().__class__.__name__,
                                     freq,
                                     irf.zero_count,
                                     irf.total_cells,
                                     irf.mse,
                                     irf.mae,
                                     irf.r2,
                                     irf.data_len])
                results_fits_vest.append([l().__class__.__name__,
                                          freq,
                                          fv_irf.zero_count,
                                          fv_irf.total_cells,
                                          fv_irf.mse,
                                          fv_irf.mae,
                                          fv_irf.r2,
                                          fv_irf.data_len])
                results_baseline.append([l().__class__.__name__,
                                         freq,
                                         irf_baseline.zero_count,
                                         irf_baseline.total_cells,
                                         irf_baseline.mse,
                                         irf_baseline.mae,
                                         irf_baseline.r2,
                                         irf_baseline.data_len])
                results_vest.append([l().__class__.__name__,
                                     freq,
                                     rf_vest.zero_count,
                                     rf_vest.total_cells,
                                     rf_vest.mse,
                                     rf_vest.mae,
                                     rf_vest.r2,
                                     rf_vest.data_len])

        df_fits = pd.DataFrame(results_fits,
                               columns=['learner', 'frequency', 'zero_count', 'total_cells', 'mse', 'mae', 'r2',
                                        'data_len'])
        df_fits_vest = pd.DataFrame(results_fits_vest,
                                    columns=['learner', 'frequency', 'zero_count', 'total_cells', 'mse', 'mae', 'r2',
                                             'data_len'])
        df_baseline = pd.DataFrame(results_baseline,
                                   columns=['learner', 'frequency', 'zero_count', 'total_cells', 'mse', 'mae', 'r2',
                                            'data_len'])
        df_vest = pd.DataFrame(results_vest,
                               columns=['learner', 'frequency', 'zero_count', 'total_cells', 'mse', 'mae', 'r2',
                                        'data_len'])
        df_fits.to_csv('fits_results_dandak.csv', index=False)
        df_fits_vest.to_csv('fv_results_dandak.csv', index=False)
        df_baseline.to_csv('baseline_results_dandak.csv', index=False)
        df_vest.to_csv('vest_results_dandak.csv', index=False)

    def test_wanxiang(self):
        # Arrange
        df = pd.read_csv(self.wanxiang)

        schema = {'Age(AD)': 'timestamp',
                  'Distance(mm)': 'numeric',
                  'd18O(VPDB)': 'numeric'}

        target = 'd18O(VPDB)'
        timestamp = 'Age(AD)'
        # df[timestamp] = df[timestamp].apply(lambda d: pd.Timestamp(1950, 1, 1) + pd.DateOffset(hours=d))

        df[timestamp] = df[timestamp].apply(lambda d: self.convert_hour(d))
        learners = [Lasso, RandomForestRegressor]

        results_fits = []
        results_baseline = []
        results_vest = []
        results_fits_vest = []

        for f in np.arange(1,
                           7, 1):
            freq = str(f) + 'H'
            for l in learners:
                print("#############################")
                print(l, " , ", freq)
                print("#############################")

                rf_vest = VESTForecaster(schema=schema,
                                         target=target,
                                         frequency=freq,
                                         learning_algorithm=l,
                                         lag_size=6,
                                         resampling_strategy='mean',
                                         imputation_strategy='ffill')

                print("=> VEST")
                rf_vest.fit(df.copy())
                # rf_vest.forecast()

                y = df[target]
                X = df.drop(target, axis=1)
                irf = AutoFITS(schema,
                               frequency=freq,
                               target=target,
                               learning_algorithm=l,
                               resampling_strategy='mean',
                               percentile=80,
                               lag_size=6,
                               imputation_strategy='ffill')

                irf_baseline = AutoFITS(schema,
                                        method='baseline',
                                        frequency=freq,
                                        learning_algorithm=l,
                                        resampling_strategy='mean',
                                        target=target,
                                        lag_size=6,
                                        imputation_strategy='ffill')
                fv_irf = AutoFV(schema,
                                frequency=freq,
                                target=target,
                                learning_algorithm=l,
                                aggregating_strategy='mean',
                                percentile=80,
                                lag_size=6,
                                imputation_strategy='ffill')

                print("\n=> AutoFITS")
                irf.fit(X, y)
                # irf.forecast()
                print("=> FITS_VEST")
                fv_irf.fit(X, y)
                # fv_irf.forecast(X, y)
                print("=> Baseline")
                irf_baseline.fit(X, y)
                # irf_baseline.forecast(X, y)
                print('')

                results_fits.append([l().__class__.__name__,
                                     freq,
                                     irf.zero_count,
                                     irf.total_cells,
                                     irf.mse,
                                     irf.mae,
                                     irf.r2,
                                     irf.data_len])
                results_fits_vest.append([l().__class__.__name__,
                                          freq,
                                          fv_irf.zero_count,
                                          fv_irf.total_cells,
                                          fv_irf.mse,
                                          fv_irf.mae,
                                          fv_irf.r2,
                                          fv_irf.data_len])
                results_baseline.append([l().__class__.__name__,
                                         freq,
                                         irf_baseline.zero_count,
                                         irf_baseline.total_cells,
                                         irf_baseline.mse,
                                         irf_baseline.mae,
                                         irf_baseline.r2,
                                         irf_baseline.data_len])
                results_vest.append([l().__class__.__name__,
                                     freq,
                                     rf_vest.zero_count,
                                     rf_vest.total_cells,
                                     rf_vest.mse,
                                     rf_vest.mae,
                                     rf_vest.r2,
                                     rf_vest.data_len])

        df_fits = pd.DataFrame(results_fits,
                               columns=['learner', 'frequency', 'zero_count', 'total_cells', 'mse', 'mae', 'r2',
                                        'data_len'])
        df_fits_vest = pd.DataFrame(results_fits_vest,
                                    columns=['learner', 'frequency', 'zero_count', 'total_cells', 'mse', 'mae', 'r2',
                                             'data_len'])
        df_baseline = pd.DataFrame(results_baseline,
                                   columns=['learner', 'frequency', 'zero_count', 'total_cells', 'mse', 'mae', 'r2',
                                            'data_len'])
        df_vest = pd.DataFrame(results_vest,
                               columns=['learner', 'frequency', 'zero_count', 'total_cells', 'mse', 'mae', 'r2',
                                        'data_len'])
        df_fits.to_csv('fits_results_wanxiang.csv', index=False)
        df_fits_vest.to_csv('fv_results_wanxiang.csv', index=False)
        df_baseline.to_csv('baseline_results_wanxiang.csv', index=False)
        df_vest.to_csv('vest_results_wanxiang.csv', index=False)

    def test_auto_correlation(self):
        # Arrange
        df = pd.read_csv(self.air_reserve, encoding='latin-1')

        schema = {'air_store_id': 'sub_series_id',
                  'visit_datetime': 'timestamp',
                  'reserve_datetime': 'date',
                  'reserve_visitors': 'numeric'}

        ts_col = df['visit_datetime'].copy()
        df['visit_datetime'] = pd.to_datetime(ts_col)
        df.sort_values(by=['visit_datetime'], inplace=True)

        for f in range(1, 32):
            freq = str(f) + 'D'
            y = df['reserve_visitors']
            X = df.drop('reserve_visitors', axis=1)
            irf = AutoFITS(schema,
                           method='baseline',
                           frequency=freq,
                           target='reserve_visitors',
                           cache_data=True)

            irf.process_data(X, y)

            subsets, new_y = irf.cached_subsets, irf.cached_y

            data = np.concatenate((subsets[0]['reserve_visitors'].values, new_y))

            # # Adding plot title.
            # plt.title("Autocorrelation Plot: " + freq)
            #
            # # Providing x-axis name.
            # plt.xlabel("Lags")
            #
            # # Plotting the Autocorreleation plot.
            # plt.acorr(data, maxlags=20)
            #
            # plt.grid(True)
            x = pd.plotting.autocorrelation_plot(data)
            x.set_xlim([0, 20])

            # ploting the Curve
            x.plot()

            plt.savefig("autocorr_" + freq + ".png")

            plt.clf()

    def test_m5_proccess_data(self):
        hist_item_sales = pd.read_csv(self.m5 + 'sales_train_validation.csv')

        start_date = datetime.datetime(2011, 1, 29)

        cols = list(hist_item_sales.columns)
        ts_cols = [i for i in cols if i.startswith('d_')]
        other_cols = [i for i in cols if i not in ts_cols]

        # days = [start_date + datetime.timedelta(days=int(i.split('_')[1]) - 1) for i in ts_cols]

        df = pd.DataFrame(columns=other_cols + ['day'] + ['amount_sold'])
        for c in ts_cols:
            day = start_date + datetime.timedelta(days=int(c.split('_')[1]) - 1)
            print("Day: ", day)
            filtered = hist_item_sales[hist_item_sales[c] != 0].copy()
            filtered['amount_sold'] = filtered[c]
            filtered['day'] = day
            df = df.append(filtered[other_cols + ['day'] + ['amount_sold']])
        df.to_csv('new_m5.csv', index=False)

        print("Dataset length: ", len(df))
        print("Items: ", len(df['item_id'].unique()))

        one_hundred_items = list(df['item_id'].unique())[:50]
        small_df = df[df['item_id'].isin(one_hundred_items)]
        print("Small dataset length: ", len(small_df))
        small_df.sort_values(by=['id', 'day'], inplace=True)
        small_df.to_csv("new_m5_reduced.csv", index=False)

    def test_m5_with_data(self):
        # Arrange
        df1 = pd.read_csv("../../data/m5/new_m5_reduced.csv")

        frequency_lags = {'1D': 7}
        learners = [Lasso, RandomForestRegressor]
        results_fits = []
        results_baseline = []

        for store_amount in range(2, 20):

            ids = list(df1['id'].unique())
            df = df1[df1['id'].isin(ids[:store_amount])].copy()

            schema = {'id': 'sub_series_id',
                      'item_id': 'categorical',
                      'dept_id': 'categorical',
                      'cat_id': 'categorical',
                      'store_id': 'categorical',
                      'state_id': 'categorical',
                      'day': 'timestamp',
                      'amount_sold': 'numeric'}

            ts_col = df['day'].copy()
            df['day'] = pd.to_datetime(ts_col)
            df.sort_values(by=['day'], inplace=True)

            for freq, lag in frequency_lags.items():
                for l in learners:
                    print("#############################")
                    print(l, " , ", freq)
                    print("#############################")

                    y = df['amount_sold']
                    X = df.drop('amount_sold', axis=1)
                    irf = AutoFITS(schema,
                                   frequency=freq,
                                   target='amount_sold',
                                   learning_algorithm=l,
                                   lag_size=lag,
                                   percentile=80)

                    print("\n=> AutoFITS")
                    irf.fit(X, y)

                    results_fits.append([l().__class__.__name__,
                                         freq,
                                         irf.zero_count,
                                         irf.total_cells,
                                         irf.mse,
                                         irf.mae,
                                         irf.r2,
                                         irf.data_len,
                                         store_amount])

                    irf.method = "baseline"

                    print("=> Baseline")
                    irf.fit(X, y)
                    print('')
                    results_baseline.append([l().__class__.__name__,
                                             freq,
                                             irf.zero_count,
                                             irf.total_cells,
                                             irf.mse,
                                             irf.mae,
                                             irf.r2,
                                             irf.data_len,
                                             store_amount])

        df_fits = pd.DataFrame(results_fits,
                               columns=['learner', 'frequency', 'zero_count', 'total_cells', 'mse', 'mae', 'r2',
                                        'data_len', 'stores'])
        df_baseline = pd.DataFrame(results_baseline,
                                   columns=['learner', 'frequency', 'zero_count', 'total_cells', 'mse', 'mae', 'r2',
                                            'data_len', 'stores'])
        df_fits.to_csv('fits_results_m5_' + str(store_amount) + '_stores.csv',
                       index=False)
        df_baseline.to_csv('baseline_results_m5_' + str(store_amount) + '_stores.csv',
                           index=False)

    def test_vostok_imputation(self):
        # Arrange
        df = pd.read_csv(self.vostok)

        schema = {'age (yrs bp)': 'timestamp',
                  'co2': 'numeric'}

        df['age (yrs bp)'] = df['age (yrs bp)'].apply(lambda d: pd.Timedelta(np.timedelta64(d, 'h')))
        learners = [Lasso]

        results_baseline = []

        for f in range(250, 4501, 250):
            freq = str(f) + 'h'
            # freq = str(f) + 'Y'
            for l in learners:
                print("#############################")
                print(l, " , ", freq)
                print("#############################")

                y = df['co2']
                X = df.drop('co2', axis=1)

                irf_baseline = AutoFITS(schema,
                                        method='baseline',
                                        frequency=freq,
                                        learning_algorithm=l,
                                        resampling_strategy='mean',
                                        target='co2',
                                        lag_size=6,
                                        imputation_strategy='ffill')
                print("=> Baseline")
                irf_baseline.fit(X, y)
                print('')

                results_baseline.append([freq,
                                         irf_baseline.mae,
                                         irf_baseline.r2,
                                         'ffill'])

                irf_baseline = AutoFITS(schema,
                                        method='baseline',
                                        frequency=freq,
                                        learning_algorithm=l,
                                        resampling_strategy='mean',
                                        target='co2',
                                        lag_size=6,
                                        imputation_strategy='mean')
                print("=> Baseline")
                irf_baseline.fit(X, y)
                print('')

                results_baseline.append([freq,
                                         irf_baseline.mae,
                                         irf_baseline.r2,
                                         'mean'])

                irf_baseline = AutoFITS(schema,
                                        method='baseline',
                                        frequency=freq,
                                        learning_algorithm=l,
                                        resampling_strategy='mean',
                                        target='co2',
                                        lag_size=6,
                                        imputation_strategy='zero')
                print("=> Baseline")
                irf_baseline.fit(X, y)
                print('')

                results_baseline.append([freq,
                                         irf_baseline.mae,
                                         irf_baseline.r2,
                                         'zero'])

        df_baseline = pd.DataFrame(results_baseline,
                                   columns=['frequency', 'mae', 'r2',
                                            'strategy'])

        df_baseline.to_csv('baseline_impputation_' + 'vostok' + '.csv', index=False)

    def test_vostok_different_features(self):
        # Arrange
        df = pd.read_csv(self.vostok)
        freq = '2250H'
        schema = {'age (yrs bp)': 'timestamp',
                  'co2': 'numeric'}

        df1 = df.copy()
        df['age (yrs bp)'] = df['age (yrs bp)'].apply(lambda d: pd.Timedelta(np.timedelta64(d, 'h')))

        y = df['co2']
        X = df.drop('co2', axis=1)
        irf = AutoFITS(schema,
                       frequency=freq,
                       target='co2',
                       resampling_strategy='mean',
                       percentile=80,
                       lag_size=6,
                       imputation_strategy='ffill')

        print("\n=> AutoFITS")
        X1, Y1 = irf.process_data(X.copy(),
                                  y.copy())

        df = df1
        df['age (yrs bp)'] = df['age (yrs bp)'].apply(lambda d: pd.Timestamp(1950, 1, 1) + pd.DateOffset(hours=d))

        y = df['co2']
        X = df.drop('co2', axis=1)
        irf = AutoFITS(schema,
                       frequency=freq,
                       target='co2',
                       resampling_strategy='mean',
                       percentile=80,
                       lag_size=6,
                       imputation_strategy='ffill')

        print("\n=> AutoFITS")
        X2, Y2 = irf.process_data(X.copy(),
                                  y.copy())

        print("Done")
