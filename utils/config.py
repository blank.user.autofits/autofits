DEBUG_PRINTS = False
DEBUG_DATASETS = False


def dprint(*args):
    if DEBUG_PRINTS:
        print(*args)


def save_frame(df,
               name,
               index=False):
    if DEBUG_DATASETS:
        df.to_csv(name, index=index)
